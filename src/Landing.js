import React, { Component } from 'react';
import {AppRegistry, View, StyleSheet, ToastAndroid,Text, Image,BackHandler, Navigator, TextInput, TouchableHighlight, Button, ImageBackground } from 'react-native';
import {Actions} from 'react-native-router-flux';


export default class Landing extends Component {

    constructor(){
        super()
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    handleBackButton(){
        BackHandler.exitApp()
        return  true;
    }


    render() {
     //   const { navigate } = this.props.navigation;

           const goToLogin = () => {
                   Actions.login()
                  // Actions.appmenu({type:Menu.RESET})


               }

               const goToSignUp = () => {
                   Actions.signup()

               }

        return (



            <View style={styles.main}>



                    <Image style={styles.backgroundImage} source = {require('./ImagesSources/LandingScreen3.png')}>
                    <View style={styles.buttons}>
                    <TouchableHighlight onPress = {goToLogin} style={styles.bStyle}>
                        <Text style = {styles.fonts}> Login </Text>
                    </TouchableHighlight>
                    <TouchableHighlight onPress = {goToSignUp} style={styles.bStyle1}>
                        <Text style = {styles.fonts}> SignUp </Text>
                    </TouchableHighlight>


                </View>
                    </Image>
                
            </View>






        );
    }
};

const styles = StyleSheet.create({
   main:{
       flex: 1,
       width: View.width,
       height: View.height,


   },

    backgroundImage:{
    flex: 1,
        resizeMode: 'cover',
        width: View.width,
        height: View.height,
        justifyContent: 'flex-end',

    },

    buttons:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        position:"absolute",
      

    },

   bStyle:{
       backgroundColor:'#023c68',
        borderColor: '#F9A402',
       borderWidth: 2,
       width: '50%',
       height:60,
       alignItems: 'center',
       justifyContent: 'center',



   } ,

    bStyle1:{
        backgroundColor:'#023c68',
        borderColor: '#F9A402',
        borderWidth: 2,
        width: '50%',
        height:60,
        alignItems: 'center',
        justifyContent: 'center',



    } ,
    fonts:{
       color:'white',
    },



});

