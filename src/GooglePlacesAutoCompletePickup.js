// import React , {Component} from 'react'
// import {AsyncStorage, BackHandler} from 'react-native'
// import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
// import {Actions} from 'react-native-router-flux'

// export default class GooglePlacesAutoCompletePickup extends Component{

//     componentDidMount(){
//         // AsyncStorage.removeItem('FormatedAddress')
//         // AsyncStorage.removeItem('GooglePlacepickupLatitude')
//         // AsyncStorage.removeItem('GooglePlacepickupLongitude')

//         BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

//     }

//     componentWillMount(){
//         BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
//     }



//     handleBackButton(){
//         Actions.appmenu()
//         return true
//     }

//     render(){
//         return(
//             <GooglePlacesAutocomplete
//                 placeholder='Search'
//                 minLength={2} // minimum length of text to search
//                 autoFocus={false}
//                 returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
//                 listViewDisplayed='auto'    // true/false/undefined
//                 fetchDetails={true}
//                 renderDescription={row => row.description} // custom description render
//                 onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true

//                     Actions.appmenu()
//                    // Actions.appmenu({type:GooglePlacesAutoCompletePickup.RESET})

//                     var pickupLatitude = String(details.geometry.location.lat);
//                     var pickupLongitude = String(details.geometry.location.lng);
//                     var FormattedAddress = data.description

//                     console.log(pickupLatitude)
//                     console.log(pickupLongitude)

//                     AsyncStorage.setItem("FormatedAddress", FormattedAddress )
//                     AsyncStorage.setItem('pickupLatitude',pickupLatitude);
//                     AsyncStorage.setItem('pickupLongitude',pickupLongitude);

//                 }}

//                 getDefaultValue={() => ''}

//                 query={{
//                     // available options: https://developers.google.com/places/web-service/autocomplete
//                     key: 'AIzaSyDCZKzKuueFu5uOfOElXWljltkD9xonTJY',
//                     language: 'en', // language of the results
//                     //types: '(geocode)' // default: 'geocode'
//                 }}

//                 styles={{
//                     textInputContainer: {
//                         width: '100%'
//                     },
//                     description: {
//                         fontWeight: 'bold'
//                     },
//                     predefinedPlacesDescription: {
//                         color: '#1faadb'
//                     }
//                 }}

//             />

//         )
//     }
// }