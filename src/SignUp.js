import React, { Component } from 'react';
import {
    AppRegistry,
     StyleSheet,
     Text,
    TextInput,
    Image,
     View,
    ScrollView,
    KeyboardAvoidingView,
    BackHandler,
    TouchableHighlight,
    Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {signupAction} from './redux/actions/signupAction';




class SignUp extends Component {
    constructor(){
        super()
        this.state = {
            userName:'',
            email:"",
            phoneNumber:"",
            password:"",
            reptPassword:"",
        }
    }

    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    validatePassword = (password) =>{
        var re = /^.{6,}/
        return re.test(password)
    };
    validatePhone = (phoneNumber) => {
        var re = /^.{9,}/
        return re.test(phoneNumber)
    };
    validateName = (userName) =>{
        var re =  /^[a-zA-Z ]{2,30}$/
        return re.test(userName)
    }


    handleSignup(){
        const signup = {
            userName:this.state.userName,
            email: this.state.email,
            phoneNumber: this.state.phoneNumber,
            password: this.state.password,
            reptPassword: this.state.reptPassword,


        }
        this.props.signupAction(signup)
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){


        Actions.landing()
        return true;
    }


    CheckingCondition(){
        if(this.state.password != this.state.reptPassword){
            alert("Password is not Matched")

        }else{
            this.handleSignup()
            Alert.alert("User Create Successfully")
            Actions.login()

        }
    }

    render() {


        const goToLogin = () => {
            Actions.login()



        }

        const goToBack = () => {
            Actions.landing()

        }

        return (
            <View style={styles.main}>

                <View style={styles.headbar}>
                    <View style={styles.backiconview}>
                        <TouchableHighlight onPress = {goToBack}>
                            <Image style={styles.backicon} source = {require('./ImagesSources/back.png')}/>
                        </TouchableHighlight>
                    </View>

                    <View style={styles.headtextview} >
                        <Text style={styles.headtext}>SIGN UP</Text>
                    </View>
                </View>
            <ScrollView>






                <KeyboardAvoidingView style={styles.form}>


                    <View style={styles.naming}>


                            <Image style={styles.icon} source = {require('./ImagesSources/user.png')}/>

                    <TextInput style={styles.Inputtext}
                               placeholder="Username"
                               placeholderTextColor = "silver"
                               autoCapitalize="words"
                               onChangeText={(text) => this.setState({userName:text})}
                               returnKeyType = {"next"}
                               onSubmitEditing={(event) => {
                                   this.refs.ThirdInput.focus();
                               }}

                    />

                    </View>

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('./ImagesSources/email.png')}/>
                    <TextInput style={styles.Inputtext}
                               placeholder="E-mail Address"
                               placeholderTextColor="silver"
                               autoCapitalize="none"
                               keyboardType="email-address"
                               onChangeText={(text) => this.setState({email:text})}
                               ref="ThirdInput"
                               returnKeyType = {"next"}
                               onSubmitEditing={(event) => {
                                   this.refs.ForthInput.focus();
                               }}
                    />
                    </View>

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('./ImagesSources/phone.png')}/>
                    <TextInput style={styles.Inputtext}
                               placeholder="Phone Number"
                               placeholderTextColor="silver"
                               autoCapitalize="none"
                               keyboardType="phone-pad"
                               onChangeText={(text) => this.setState({phoneNumber:text})}
                               ref="ForthInput"
                               returnKeyType = {"next"}
                               onSubmitEditing={(event) => {
                                   this.refs.FifthInput.focus();
                               }}
                    />
                    </View>

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('./ImagesSources/password.png')}/>
                    <TextInput style={styles.Inputtext}
                               placeholder="Password"
                               placeholderTextColor="silver"
                               autoCapitalize="none"
                               secureTextEntry={true}
                               onChangeText={(text) => this.setState({password:text})}
                               ref="FifthInput"
                               returnKeyType = {"next"}
                               onSubmitEditing={(event) => {
                                   this.refs.SixthInput.focus();
                               }}
                    />
                    </View>


                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('./ImagesSources/password.png')}/>
                    <TextInput style={styles.Inputtext}
                               placeholder="Repeat Password"
                               placeholderTextColor="silver"
                               autoCapitalize="none"
                               secureTextEntry={true}
                               onChangeText={(text) => this.setState({reptPassword:text})}
                               ref="SixthInput"
                    />
                    </View>

                    <TouchableHighlight style = {styles.submitbutton} onPress = {() => {
                        if(!this.validateName(this.state.userName)){
                            alert("Kindly Input Correct UserName")
                        }
                        if (!this.validateEmail(this.state.email)) {
                            // not a valid email
                            alert("Invalid Email Address")
                        }
                        if (!this.validatePassword(this.state.password)){
                            alert("Password Must be more than 6 Characters")
                        }
                        if (!this.validatePhone(this.state.phoneNumber)){
                            alert("Phone Number is not Valid")
                        }
                        else{
                            this.CheckingCondition()
                        }
                    }}>
                        <Text style = {styles.submit}>
                            SIGN UP
                        </Text>
                    </TouchableHighlight>
                </KeyboardAvoidingView>

                    <View style={styles.image}>
                        <Image  source = {require('./ImagesSources/logo.png')}/>

                    </View>

                <View style={styles.bottom}>
                    <Text style={styles.term}>By signing up with K-RIDE you agree to our terms and conditions</Text>



            </View>

    </ScrollView>
            </View>
        )


    }
};

const styles = StyleSheet.create({
    main:{
        // marginTop:22,
        flex: 1,
       justifyContent: 'center',
        backgroundColor: '#063e6a',



    },

    icon:{
        width: 30,
        height: 30,
        marginTop: 15,

    },

    username:{

        flexDirection: 'row',

    },



    image: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'white',
        paddingTop: '10%',
        paddingBottom:'10%'




    },
    naming:{
      flexDirection:'row',
        justifyContent: 'center',
        marginTop:30,
    },
    Inputtext1:{
        color:'white',
        fontSize: 15,
        height: 50,
        width: '40%',
        paddingRight: 10,
        paddingLeft: 10,

    },
    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: '10%',
       flexDirection: 'row',
    },

    headtextview:{
        justifyContent: 'center',



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },

    backiconview:{

        justifyContent: 'center',


    },
    backicon:{
        width:50,
        height:50
    },





    form: {
            justifyContent:'center',
            alignItems:'center',

        },
        Inputtext: {
            color:'white',
            fontSize: 15,
            margin: 3,
            height: 50,
            width: '80%',

            paddingRight: 10,
            paddingLeft: 10,

        },
        submitbutton: {
            justifyContent: 'center',
            //borderRadius:5,
            margin: 10,
            width: '70%',
            height: '15%',
            alignItems: 'center',
            borderWidth: 5,
            borderColor: '#ffa802',
            backgroundColor: '#003A67',
        },
        submit: {
          color:'white',
          fontSize:30,


        },
    bottom:{
      backgroundColor:'#003A67',
        alignItems:'center',

    },
    term:{
        color:'white',
        fontSize:15,
        paddingBottom:'20%',
    }




    });
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        signupAction
    },dispatch)

}
export default connect(null,mapDispatchToProps)(SignUp)
