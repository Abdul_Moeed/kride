import React, { Component } from 'react';
import {
    AppRegistry,
    TouchableHighlight,
    View,
    StyleSheet,
    TextInput,
    Image,
    AsyncStorage,
    Alert,
    Dimensions,
    TouchableOpacity,
    Navigation,
    Text,
    BackHandler
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, MapMarker, showMyLocationButton} from 'react-native-maps';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {pickupAction } from '../redux/actions/pickupAction';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import PopupDialog from 'react-native-popup-dialog'

let {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class Mapclass extends Component {

    constructor() {
        super();
        

        this.state = {
            region:{
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            },
            seachedAddress:'Karachi',
            statusBarHeight:10,
        }

    }

    // _findMe(){
    //    return(
    //     navigator.geolocation.getCurrentPosition(
    //         position => {
    //             this.setState({
    //                 region:{
    //                     latitude: position.coords.latitude,
    //                     longitude: position.coords.longitude,
    //                     latitudeDelta: LATITUDE_DELTA,
    //                     longitudeDelta: LONGITUDE_DELTA
    //                 }
    //             });
    //         },
    //         (error) => console.log(error.message),
    //         {
    //             enableHighAccuracy:true , timeout :20000, maximumAge:1000
    //         }
    //     )
             
    //    )
    //   }

    placePickerDialog(){
        return(
            <View style={{width:"100%", height:"100%"}}>
                <GooglePlacesAutocomplete
                placeholder='Search'
                minLength= {2}
                autoFocus={false}
                returnKeyType={'done'}
                listViewDisplayed='auto'
                fetchDetails={true}
                renderDescription={row => row.description}
                onPress={(data, details = null) => {    
                    this.setState({
                        region:{
                            latitude: details.geometry.location.lat,
                            longitude: details.geometry.location.lng,
                            latitudeDelta: 0.01,
                            longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
                        },
                        seachedAddress : data.description
                    });
                   


                  this.searchDialog.dismiss()
                   

                } }

                getDefaultValue={()=> ''}

                query ={{
                    key : 'AIzaSyDCZKzKuueFu5uOfOElXWljltkD9xonTJY',
                    language:'en'
                }}

                styles={{
                    textInputContainer: {
                        width: '100%'
                    },
                    description: {
                        fontWeight: 'bold'
                    },
                    predefinedPlacesDescription: {
                        color: '#1faadb'
                    }
                }}


                />
            </View>
        )
    }

   

    componentDidMount(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    region:{
                        latitude:position.coords.latitude,
                        longitude:position.coords.longitude,
                        latitudeDelta:LATITUDE_DELTA,
                        longitudeDelta:LONGITUDE_DELTA
                    },
                    seachedAddress:'Current Location'
                })
            }
        ),
        (error) => alert('Location is not Provided'),
        {enableHighAccuracy:false, timeout:20000, maximumAge:1000}
        
        

       BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        
       
    
    // this.watchID = navigator.geolocation.watchPosition(
    //     position => {
    //         // if (this.state.region.latitude === LATITUDE){
    //             this.setState({
    //                 region:{
    //                     latitude: position.coords.latitude,
    //                     longitude: position.coords.longitude,
    //                     latitudeDelta: LATITUDE_DELTA,
    //                     longitudeDelta: LONGITUDE_DELTA,
    //                 },
    //                 seachedAddress:'Current Location'
    //             })
    //             navigator.geolocation.clearWatch(this.watchID)
    //         // }
    //     }
    // )
       
    }

   

    // componentWillReceiveProps(){
    //     var pickupLatitude = String(this.state.latitude)
    //     var pickupLongitude = String(this.state.longitude)

    //     AsyncStorage.setItem('pickupLatitude', pickupLatitude)
    //     AsyncStorage.setItem('pickupLongitude', pickupLongitude)

    //     console.log('Pickup Location', this.state.latitude)
    // }




    componentWillMount(){

        setTimeout(()=>this.setState({statusBarHeight: 0}),500);
         }
    componentWillUnmount(){
        navigator.geolocation.clearWatch(this.watchID)
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
    }

    handleBackButton(){
        this.searchDialog.dismiss()
    }


//     onRegionChangeComplete = (MAP) => {

//         this.setState({
//             latitude:MAP.latitude,
//             longitude:MAP.longitude,
//             latitudeDelta:MAP.latitudeDelta,
//             longitudeDelta:MAP.longitudeDelta
//         })

    // var pickupLatitude = String(MAP.latitude)
    // var pickupLongitude = String(MAP.longitude)

    // AsyncStorage.setItem('pickupLatitude',pickupLatitude)
    // AsyncStorage.setItem('pickupLongitude',pickupLongitude)


//     console.log('dropoffLatitude SETITEM:',pickupLatitude)
//     console.log('dropoffLongitude SETITEM:',pickupLongitude)



// }


    USERMAP(){

      
            return(
                <MapView style={styles.second }
                         provider={PROVIDER_GOOGLE}
                         showsUserLocation={true}
                         showsBuildings={true}
                         followUserLocation={true}
                         region={this.state.region}
                         zoomEnabled={true}
                         onRegionChangeComplete={(region) => {
                                    latitude:region.latitude
                                    longitude:region.longitude

                                    var pickupLatitude = String(region.latitude)
                                    var pickupLongitude = String(region.longitude)
                                
                                    AsyncStorage.setItem('pickupLatitude',pickupLatitude)
                                    AsyncStorage.setItem('pickupLongitude',pickupLongitude)

                                    
                         }}

                >
                
                </MapView>
            )
    }




    render() {

        
        

        return(
            <View style={{marginBottom : this.state.statusBarHeight,
                flex: 1,
                justifyContent:"center",
                alignItems:"center",}}>
            <PopupDialog
                        ref={(popupDialog) => { this.searchDialog = popupDialog; }}
                    >
                        {this.placePickerDialog()}
                    </PopupDialog>
                    <TouchableOpacity
                    style={{width:'100%', height: 50, backgroundColor:'silver', position:'absolute', top: 0, alignItems:'center', flexDirection:'row'}}
                    onPress={()=>{
                            this.searchDialog.show();
                        }}>
                        <Image style={{height:30, width:30, marginLeft:8}} source={require('../ImagesSources/search-icon.png')}/>
                        <Text style={{fontSize:16}}> Search: {this.state.seachedAddress}</Text>
                        </TouchableOpacity>

                        {this.USERMAP()}

                <Image style={{marginTop:10, height:50, width:50}} source={require('../ImagesSources/userLocation.png')}/>
            </View>

        )
    }
}
const styles = StyleSheet.create({

    // main:{
        
    // },
    

    second:{
        position: 'absolute',
        top: 50,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -5,
    },
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators({pickupAction}, dispatch)
}
function mapStateToProps(state) {
    return{
        region:state.region.region
    }
}
export default connect (mapStateToProps, mapDispatchToProps) (Mapclass)