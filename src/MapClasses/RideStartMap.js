import React, { Component } from 'react';
import {
    AppRegistry,
    TouchableHighlight,
    View,
    StyleSheet,
    Text,
    Image,
    AsyncStorage,
    Alert
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, MapMarker} from 'react-native-maps';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {pickupAction } from '../redux/actions/pickupAction';
import firebaselocation from '../firebase'

class RideStartMap extends Component{
    constructor(){
        super()
        this.state = {
            driverid:'',
            listingData:[]
        }

    }

    componentDidMount(){
        this.props.RideStatus.map((X) => {
            let driverid = X.data._id
            this.setState({driverid:driverid})
        })
    }

    componentWillMount(){
       this.intervalFirebase=setInterval(()=> {
            let q = firebaselocation.database().ref();
            q.on('value', (snap) => {
                var finished = []
                snap.forEach((data) => {
                    let result = data.val();
                    result[this.state.driverid] = data.key;
                    finished.push(result);
                })
                // alert(finished)
                this.setState({
                    listingData: finished
                })
            })
        },3000)
    }

    componentWillUnmount(){
        clearInterval(this.intervalFirebase)
    }

    USERMAP(){

        return this.props.region.map((MAP) => {
            return(
                <MapView style={styles.second}
                         key={MAP}
                         provider={PROVIDER_GOOGLE}
                         showsUserLocation={true}
                         showsBuildings={true}
                         showsMyLocationButton={true}
                         followUserLocation={true}
                         initialRegion={MAP}
                         zoomEnabled={true}
                >
                    {
                        this.state.listingData.map((x) => (
                            <MapView.Marker  coordinate={{latitude:x.Latitude,
                                longitude:x.Longitude}}>

                            </MapView.Marker>
                        ))
                    }

                </MapView>
            )
        })
    }
    render(){
        return(
            <View style={styles.main}>
                {this.USERMAP()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main:{
        flex: 1,
        justifyContent:"center",
        alignItems:"center"
    },

    second:{

        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
})


function mapDispatchToProps(dispatch) {
    return bindActionCreators({pickupAction}, dispatch)
}
function mapStateToProps(state) {
    return{
        region:state.region.region,
        RideStatus:state.RideStatus.RideStatus
    }
}
export default connect (mapStateToProps, mapDispatchToProps) (RideStartMap)