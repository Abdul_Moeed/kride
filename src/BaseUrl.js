
// const BaseURL = 'http://192.168.10.12:4000/'         // Local IP
// const BaseURL = 'http://env-2686917.whelastic.net/'  // WebHosting Elastic
const BaseURL = 'https://nodeapp12345.herokuapp.com/'  // heroku hosting



export const LoginUrl = BaseURL+'login/User';
export const SignupUrl = BaseURL+'user/api';
export const userRideListInfo = BaseURL+'user/getalluserRides';
export const RideNowURL = BaseURL+'rides/api';
export const RideLaterURL = BaseURL+'rides/RideLater';
export const UserInfoUrl = BaseURL+'user/singleuser';
export const RIDESTATUSURL = BaseURL+'user/ridestatus'
export const ADDCREDITURL = BaseURL+'creditcard/api';
export const MyBooking = BaseURL+'rides/BookRide'
export const billUrl = BaseURL+'user/Result';
export const ratingUrl = BaseURL+'user/RateCaptain'
export const UpdateuserUrl = BaseURL+'user/UpdateUser'
export const ImageUploadUrl = BaseURL+'user/upload'