import React, { Component } from 'react';
import { ActivityIndicator, View,AsyncStorage, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import {RideStatusAction} from './redux/actions/RideStatusAction'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {Actions} from 'react-native-router-flux'



class ActivityIndicatorExample extends Component {
    constructor(){
        super()

        this.state = {
            Rideid:''
        }
    }
    state = { animating: true }

    closeActivityIndicator = ()=> setTimeout(() => this.setState({animating: false }), 6000)




    componentDidMount(){
        this.RideIdHitting()
    }

    RideIdHitting(){
        this.IntervalOne = setInterval(()=>{
            this.props.RideNow.map((x) => {
                AsyncStorage.setItem('RIDEID', x.data.Rideid)
                const RideStatus = {
                    rideid: x.data.Rideid
                }
                this.props.RideStatusAction(RideStatus)
                // console.log(RideStatus)
            })

            this.ServerResponse()
        }, 3000)
    }

    ServerResponse(){
        this.Interval = setInterval(()=> {
            this.props.RideStatus.map((x) => {
                let RIDEstatus = String(x.RideStatus)
                AsyncStorage.setItem('RIDESTATUS' ,RIDEstatus)
                if(x.RideStatus === 1 ){
                    this.goToRideStart()
                }
            })
        },5000)
    }


    goToRideStart(){
        this.closeActivityIndicator();
        Actions.ridestart()
        clearInterval(this.IntervalOne)
        clearInterval(this.Interval);
    }

    render() {


        const animating = this.state.animating
        return (
            <View style = {styles.container}>
                <Text style={styles.text}> Waiting for Driver</Text>
                {/* <ActivityIndicator
                    animating = {animating}
                    color = '#bc2b78'
                    size = "large"
                    style = {styles.activityIndicator}/> */}
                    <Image source={require('./ImagesSources/loading.gif')} />
            </View>
        )
    }
}

const styles = StyleSheet.create ({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '30%',
        backgroundColor:'white'
    },
    text:{
        fontWeight:'bold',
        color:'#4286f4',
        fontSize:22,        
    },
})

function mapStateToProps(state) {
    return{

        RideStatus:state.RideStatus.RideStatus,
        RideNow:state.RideNow.RideNow
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

        RideStatusAction

    }, dispatch)
}


export default connect (mapStateToProps, mapDispatchToProps) (ActivityIndicatorExample)