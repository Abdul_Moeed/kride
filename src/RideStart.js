import React, {Component} from 'react';
import {View, StyleSheet,TouchableHighlight,Image,Text,AsyncStorage} from 'react-native';
import RideStartMap from './MapClasses/RideStartMap';
import {connect} from 'react-redux';
import {RideStatusAction} from './redux/actions/RideStatusAction'
import {bindActionCreators} from 'redux'
import Communications from 'react-native-communications';
import {Actions} from 'react-native-router-flux'

class RideStart extends Component{
    constructor(){
        super()
    }

    componentDidMount(){
        this.ServerResponse()
        this.IntervalOne = setInterval(()=>{
               AsyncStorage.getItem('RIDEID').then((RIDEID) => {
                   const RideStatus = {
                       rideid: RIDEID
                   }
                   this.props.RideStatusAction(RideStatus)
               })
        }, 2000)
    }

    ServerResponse(){
        this.IntervalTwo = setInterval(()=> {
            this.props.RideStatus.map((x) => {
                // console.log('RIDE START WALA DATA', x.RideStatus)
                let RIDEstatus = String(x.RideStatus)
                AsyncStorage.setItem('RIDESTATUS' ,RIDEstatus)
                if(x.RideStatus === 5){
                    Actions.billing()
                    clearInterval(this.IntervalOne)
                    clearInterval(this.IntervalTwo)
                    
                }
            })
        },5000)
    }



    render(){


        return(

            <View style={styles.container}>

                <View style={styles.headbar}>


                    <View style={styles.headtextview} >
                        <Text style={styles.headtext}>RIDE START</Text>
                    </View>


                </View>

                <RideStartMap/>

                    {
                        this.props.RideStatus.map((x) => {

                            return(

                                <View key={x.data._id} style={styles.bottomView}>
                                    <View style={styles.ImageView}>
                                        <Image
                                            style={styles.imageDriver}
                                            // source={{uri:'http://192.168.0.121:4000/captain/getimage?image_name=59ddd7d52a6bf024dc94a62e.jpeg'}}
                                            source={require('./ImagesSources/man.png')}
                                        />
                                    </View>

                                    <View style={styles.userinfobuttonView}>
                                        <View style={styles.userinfoView}>
                                            <Text style={{marginLeft:'20%', fontWeight:'500', color:'black'}}>{x.data.captainName}</Text>
                                        </View>
                                        <View style={{flexDirection:'row',marginTop:2, marginLeft:35}}>
                                            <TouchableHighlight style={styles.callButtonView} onPress={() => Communications.phonecall(x.data.phoneNumber, true)}>
                                                <Text style={{color:'white'}}>CALL CAPTAIN</Text>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                </View>
                            )
                        })
                    }

            </View>
        )
    }
}

const styles = StyleSheet.create({
    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,

        flexDirection: 'row',
    },



    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,
        marginLeft:20



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },


    button: {
        justifyContent: 'center'

    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    container: {
        // marginTop:22,
        flex: 1,
    },
    bottomView:{
        borderColor:'black',
        borderWidth:3,
        flexDirection:'row',
        height:'25%',
        width:'100%',
        backgroundColor:'#c9e7ff'
    },
    ImageView:{
        borderColor:'#032f53',
        borderWidth:3,
        height:'100%',
        width:'40%',
        borderRadius:75,
        justifyContent:'center',
        alignItems:'center'
    },
    userinfoView:{
        // borderColor:'black',
        // borderWidth:3,
        height:'50%',
        width:'100%',
        justifyContent:'center',
    },

    userinfobuttonView:{
        // borderColor:'black',
        // borderWidth:3,
        height:'100%',
        width:'100%',
        flexDirection:'column'
    },
    callButtonView:{
        height:50,
        width:110,
        borderColor:'#032f53',
        backgroundColor:'#016F77',
        borderRadius:5,
        borderWidth:2,
        justifyContent:"center",
        alignItems:'center',
        margin:5
    },
    imageDriver:{
        height:125,
        width:120,
        borderRadius:75
    }

})

function mapStateToProps(state) {
    return{
        RideStatus:state.RideStatus.RideStatus
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({RideStatusAction},dispatch)
}

export default connect (mapStateToProps, mapDispatchToProps) (RideStart)