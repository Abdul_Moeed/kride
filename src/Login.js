import React, { Component } from 'react';
import {StyleSheet,
    Text,
    TextInput,
    Image,
    View,
    BackHandler,
    ScrollView,
    AsyncStorage,
    ActivityIndicator,
    Alert,
    KeyboardAvoidingView,
    TouchableHighlight,} from 'react-native';
import {Actions} from 'react-native-router-flux'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {loginAction} from './redux/actions/loginAction'
import {logoutAction} from './redux/actions/logoutAction'
import {Spinner} from './components'



class Login extends Component {

    constructor(){
        super();
        this.state = {
            email:'',
            password:'',
            loading:false
        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
     handleLogin() {
        const login = {
            email: this.state.email,
            password: this.state.password
        };
        this.props.loginAction(login);
    }

    USERLOGIN(){
        this.handleLogin()
     }

    goToAppMenu(){
        
       Actions.appmenu()
       alert('Success')
    }


    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
  

    handleBackButton(){
        Actions.landing();
        return true;
    }

    renderButton(){
        if (this.state.loading){
            return <Spinner size="small"/>;
        }
        return(
            <TouchableHighlight style = {styles.submitbutton}
                                          onPress = {() => {
                                              this.setState({loading:true})
                                              if(!this.validateEmail(this.state.email)){
                                                return  Alert.alert("Please write correct Email Address")
                                              }else{
                                                 return   this.handleLogin()

                                              }
                                          }}>
                            <Text style = {styles.submit}>
                                LOGIN
                            </Text>
                        </TouchableHighlight>
        )
    }

    render() {
        
        this.props.login.map((x) => {
            
            if (x.status === 0) {
               Alert.alert("Failed")
                    this.setState({loading:false})
                this.props.logoutAction()


            } else
            {
                AsyncStorage.setItem("userName", x.data.Userdata.userName)
                AsyncStorage.setItem('_id', x.data.Userdata._id)
                AsyncStorage.setItem("phoneNumber",x.data.Userdata.phoneNumber)
                AsyncStorage.setItem("email", x.data.Userdata.email)
                AsyncStorage.setItem("password",x.data.Userdata.password)
                 this.goToAppMenu() 

            }
        })

        const goToBack = () => {
            Actions.landing()
        };




        return (
            <View style={styles.main}>
                <View style={styles.headbar}>

                    <View style={styles.backiconview}>
                        <TouchableHighlight  onPress = {goToBack}>
                        <Image style={styles.backicon} source = {require('./ImagesSources/back.png')}/>
                        </TouchableHighlight>
                    </View>

                    <View style={styles.headtextview}>
                        <Text style={styles.headtext}>LOGIN</Text>
                    </View>
                    </View>

                <ScrollView>


                    <KeyboardAvoidingView style={styles.form}>
                        <View style={styles.username}>
                        <Image style={styles.icon} source = {require('./ImagesSources/email.png')}/>

                        <TextInput style={styles.Inputtext}
                                   placeholder="E-mail Address"
                                   placeholderTextColor="silver"
                                   autoCapitalize="none"
                                   keyboardType="email-address"
                                   onChangeText={(text) => this.setState({email:text})}
                                   returnKeyType = {"next"}
                                   onSubmitEditing={(event) => {
                                       this.refs.SecondInput.focus();
                                   }}

                        />
                        </View>
                        <View style={styles.username}>
                            <Image style={styles.icon} source = {require('./ImagesSources/password.png')}/>
                        <TextInput style={styles.Inputtext}
                                   ref="SecondInput"
                                   placeholder="Password"
                                   placeholderTextColor="silver"
                                   autoCapitalize="none"
                                   secureTextEntry={true}
                                   onChangeText={(text) => this.setState({password:text})}
                        />
                        </View>
                        <Text style={styles.Inputtext1}>
                            Forgot Password?
                        </Text>


                            {this.renderButton()}
                        
                    </KeyboardAvoidingView>

                    <View style={styles.image}>
                        <Image source = {require('./ImagesSources/logo.png')}/>

                    </View>




                </ScrollView>
            </View>


        )


    }
}

const styles = StyleSheet.create({
    main:{
        // marginTop:22,
        flex:1,
        justifyContent: 'center',
        backgroundColor: '#063e6a',

    },
    headbar:{
      backgroundColor:'#032f53',
        width: View.width,
        height: '10%',

        flexDirection: 'row',
    },
    backicon:{
        width:50,
        height:50
    },



    headtextview:{
        justifyContent: 'center',



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },

    backiconview:{

        justifyContent: 'center',


    },





    image: {
        height:180,
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'white',
        paddingBottom: 40,
    },
    icon:{
        width: 30,
        height: 30,
        marginTop: 15,

    },

    username:{

        flexDirection: 'row',

    },


    form: {
        marginTop:'30%',
        justifyContent:'center',
        alignItems:'center',


    },
    Inputtext: {
        color:'white',
        fontSize: 15,
        margin: 3,
        height: 50,
        width: '80%',
        borderRadius: 5,
        paddingRight: 10,
        paddingLeft: 10,

    },

    Inputtext1: {
        justifyContent: 'center',
        color:'white',
        fontSize: 18,
        marginBottom: '20%',




    },
    submitbutton: {
        justifyContent: 'center',
        //borderRadius:5,
        margin: 10,
        width: '70%',
        height: '20%',
        alignItems: 'center',
        borderWidth: 5,
        borderColor: '#ffa802',
        backgroundColor: '#003A67',
    },
    submit: {
        color:'white',
        fontSize:30,


    },



});


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    loginAction,
        logoutAction
    },dispatch)
}

function mapStateToProps(state) {
    return{
        login:state.login.login
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(Login);