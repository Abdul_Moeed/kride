import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Alert,
    AsyncStorage,
    TouchableHighlight,
    BackHandler,
    ToastAndroid
} from 'react-native';
import Mapclass from './MapClasses/RideLaterMap';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {RideLaterAction} from './redux/actions/BookItRideLaterAction'
import DatePicker from 'react-native-datepicker';
import {Actions} from 'react-native-router-flux';


const image = require('./ImagesSources/back.png');

const styles = StyleSheet.create({

    blackline:{
        borderWidth :1,
        borderColor: 'black',
        marginTop: '2%',
        marginBottom: '2%'
    },

    searching:{
        backgroundColor: '#ffffff',
        width: '15%',
    },
    searchingicon:{
        marginTop: '20%',
        margin: '10%'

    },
    favouriting:{
        backgroundColor: '#ffffff',
        width: '15%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    favouritingicon:{
        margin: '10%',
    },

    viewinput:{
        backgroundColor: '#ffffff',
        alignItems: 'center',
        width: '100%',

    },

    dateandtime:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        height: 50,

    },

    inputplace:{

        width: '70%',
    },



    buttons:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',


    },

    bStyle:{
        backgroundColor:'#023c68',
        borderColor: '#F9A402',
        borderWidth: 2,
        width: '100%',
        height:50,
        alignItems: 'center',
        justifyContent: 'center',



    } ,

    fonts:{
        color:'white',
    },

    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,

        flexDirection: 'row',
    },



    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },


    button: {
        justifyContent: 'center'

    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    container: {
        // marginTop:22,
        flex: 1,
    },

});

class RideLater extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id:'',
            pickupLatitude:null,
            pickupLongitude:null,
            dropoffLatitude:null,
            dropoffLongitude:null,
            pickupTime:null,
            isDateTimePickerVisible: false,

        }
    }

    handleBookIt(){
        const RideLater = {
            userid:this.state._id,
            pickupLatitude:this.state.pickupLatitude,
            pickupLongitude:this.state.pickupLongitude,
            dropoffLatitude:this.state.dropoffLatitude,
            dropoffLongitude:this.state.dropoffLongitude,
            pickupTime:this.state.pickupTime,
        }
        this.props.RideLaterAction(RideLater)
        // console.log('ACTION TRIGER:', RideLater)
        Actions.appmenu()
    }

    Check(){
        AsyncStorage.getItem("_id").then((_id) => {
            this.setState({_id:_id})
            // console.log(_id)
        })


        AsyncStorage.getItem("pickupLatitude").then((xyz) => {
            let pickupLatitude = Number(xyz)
            this.setState({pickupLatitude:pickupLatitude})
            // console.log("pickupLatitude",pickupLatitude)
        })

        AsyncStorage.getItem("pickupLongitude").then((xyz) => {
            let pickupLongitude = Number(xyz)
            this.setState({pickupLongitude:pickupLongitude})
            // console.log("pickupLongitude",pickupLongitude)
        })
    }

    DropOffLocation(){
        AsyncStorage.getItem("dropoffLatitudeRideLater").then((xyz) => {
            let dropoffLatitudeRideLater = Number(xyz)
            this.setState({dropoffLatitude:dropoffLatitudeRideLater})
            // console.log("dropoffLatitudeRideLater",dropoffLatitudeRideLater)
        })

        AsyncStorage.getItem("dropoffLongitudeRideLater").then((xyz) => {
            let dropoffLongitudeRideLater = Number(xyz)
            this.setState({dropoffLongitude:dropoffLongitudeRideLater})
            // console.log("dropoffLongitudeRideLater",dropoffLongitudeRideLater)

            this.handleBookIt()
        })

    }

    handleTiger(){
      this.DropOffLocation()
    }


    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.Check()
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){


        ToastAndroid.show('For Back Press Top Left Button', ToastAndroid.LONG);
        return true;
    }

    render() {

        const goToMenu = () => {
            Actions.appmenu()
        }


        return (
                <View style={styles.container}>

                    <View style={styles.headbar}>

                        <TouchableHighlight
                            onPress = {goToMenu}
                            style={styles.button}
                        >
                            <Image
                                source={image}
                                style={{ width: 32, height: 32}}
                            />
                        </TouchableHighlight>

                        <View style={styles.headtextview} >
                            <Text style={styles.headtext}>RIDE LATER</Text>
                        </View>


                    </View>

                    <View style={styles.viewinput}>
                        <View style={styles.dateandtime}>
                            <DatePicker
                                style={{width: '80%'}}
                                date={this.state.pickupTime}
                                mode="datetime"
                                placeholder="Select Date & Time"
                                format="YYYY-MM-DD  HH:MM:SS"
                                minDate="2018-04-01"
                                maxDate="2020-12-31"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,

                                    },
                                    dateInput: {

                                        alignItems: 'center'

                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({pickupTime: date})}}
                            />


                        </View>

                    </View>

                    <Mapclass/>

                    <View style={styles.buttons}>
                        <TouchableHighlight onPress = {() => this.handleTiger() } style={styles.bStyle}>
                            <Text style = {styles.fonts}> SAVE</Text>
                        </TouchableHighlight>



                    </View>

                </View>

        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({RideLaterAction},dispatch)
}

function mapStateToProps(state) {
    return{
        RideLater:state.RideLater.RideLater
    }
}

export default connect (mapStateToProps, mapDispatchToProps) (RideLater)