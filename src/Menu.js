import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet,AsyncStorage, Dimensions, Image , ScrollView, BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Communications from 'react-native-communications';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {loginAction} from './redux/actions/loginAction'
import {logoutAction} from './redux/actions/logoutAction'


const window = Dimensions.get('window');







class Menu extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            _id:'',
            userName:'',
           // avatarSource:null
        }
    }

    Check(){
        AsyncStorage.getItem("_id").then((_id) => {
            this.setState({_id:_id})
            // console.log(_id)
        })

    }
    userName(){
        AsyncStorage.getItem("userName").then((userName) => {
            this.setState({userName:userName})
        })
    }


    componentDidMount(){
        this.userName()
        this.UserData()
        this.Check();
    }

    goTomyAccount(){
        Actions.myaccount()
        Actions.myaccount({type:Menu.RESET})
    }

    UserData(){
            return(
                <View style={styles.avatarContainer}>
                    <TouchableOpacity style={styles.avatarContainerButton} onPress={() => this.goTomyAccount()}>
                            <Image style={{height:50, width:50, borderRadius:25, marginRight:5}} source = {require('./ImagesSources/userIcon.png')} />
                            <Text  style={styles.name}>{this.state.userName}</Text>
                    </TouchableOpacity>
                   
                </View>
            )
    }

    render(){
        const goToMenu = () => {
            Actions.appmenu()
            Actions.appmenu({type:Menu.RESET})


        }

        const goToBooking = () => {
            Actions.booking()
            Actions.booking({type:Menu.RESET})

        }
        const goToContactus = () => {
            Actions.Contactus()
            Actions.Contactus({type:Menu.RESET})

        }
        const goToPromotions = () => {
            Actions.promotions()
            Actions.promotions({type:Menu.RESET})

        }
        const goToMyRide = () => {
            Actions.myride()
            Actions.myride({type:Menu.RESET})

        }
        const goToWallet = () => {
            Actions.wallet()
            Actions.wallet({type:Menu.RESET})

        }


        const goToLogin = () => {
            AsyncStorage.removeItem("_id").then((_id) => {
                // console.log("Logout Wala data",_id)
            })
            AsyncStorage.removeItem('image').then((picture)=>{
                // console.log("Logout Wala data",picture)
            })
            AsyncStorage.removeItem('userName')
            this.props.logoutAction()
            Actions.landing()
        }
        return (

            <View style={styles.menu}>

                {this.UserData()}

                <TouchableOpacity style={styles.button} onPress= {goToMenu} >
                    <Text style={styles.item}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress= {goToBooking}>
                    <Text style={styles.item}>My Bookings</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress= {goToMyRide}>
                    <Text style={styles.item}>My Ride</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress= {goToWallet}>
                    <Text style={styles.item}>Wallet</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress= {goToPromotions}>
                    <Text style={styles.item}>Promotions</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress= {goToContactus}>
                    <Text style={styles.item}>Contact Us</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => Communications.web('http://k-ride.com/')}>
                    <Text style={styles.item}>Help</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress= {goToLogin}>
                    <Text style={styles.item}>Log Out</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    menu: {
        // marginTop:22,
        flex: 1,
        width: '100%',
        height: window.height,
        backgroundColor: '#5691a5',
        padding: 20,
    },
    avatarContainer: {
        marginBottom: 20,
        marginTop: 20,
        height:"20%",
        // borderWidth:2,
        // borderColor:'#032f53',
        justifyContent:"center",
    },
    avatarContainerButton:{
        height:75,
        width:140,
        borderRadius: 4,
      //  borderWidth:2,
        borderColor:'#032f53',
        justifyContent:"center",
        alignItems:'center'
    },
    // avatar: {
    //     width: "100%",
    //     height: "100%",
    //     borderRadius: 44,
    //     flex: 1,
    // },
    name: {
        color:'white',
        fontWeight:'bold',
        fontSize:16,
        color:"#032f53"
    },
    item: {
        fontSize: 16,
        fontWeight: '400',
        paddingTop: 5,
        color:'#032f53',
    },
    button: {
        marginRight:'45%',
        marginBottom:15,
        borderBottomColor: '#032f53',
        borderBottomWidth: 1


    },

});


function mapStateToProps(state) {
    return{
        login:state.login.login
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        loginAction,
        logoutAction
    },dispatch)
}

export default connect (mapStateToProps, mapDispatchToProps) (Menu)


