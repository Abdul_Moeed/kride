import {PICKUPLOCATION} from './index';
import {Dimensions} from 'react-native'
const {width, height } = Dimensions.get('window');
const ASPECT_RATIO = width/height;
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = ASPECT_RATIO*LATITUDE_DELTA

export function pickupAction() {
    return (dispatch) => {
        navigator.geolocation.getCurrentPosition(
            position => {
                dispatch({type: PICKUPLOCATION, payload:{
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude,
                    latitudeDelta:LATITUDE_DELTA,
                    longitudeDelta:LONGITUDE_DELTA
                }})
            }
        ),
            (error) => alert("Your device is not providing Location"),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        navigator.geolocation.watchPosition((
            position => {
                dispatch({type: PICKUPLOCATION, payload:{
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude,
                    latitudeDelta:LATITUDE_DELTA,
                    longitudeDelta:LONGITUDE_DELTA
                }})
            }
        ))

        navigator.geolocation.clearWatch(this.watchID)
    }
}

