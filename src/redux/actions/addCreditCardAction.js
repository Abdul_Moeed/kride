"use strict"
import axios from 'axios';
import {ADDCREDIT} from './index';
import {ADDCREDITURL} from '../../BaseUrl';


export function addCreditCardAction(addCreditCard) {

    return function (dispatch) {
        axios.post(`${ADDCREDITURL}`,addCreditCard)
            .then(function (response) {
                dispatch({type: "ADDCREDIT", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "ADDCREDIT_REJECT", payload:err})
            })
    }

}
