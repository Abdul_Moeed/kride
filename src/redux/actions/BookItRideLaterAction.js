"use strict"
import axios from 'axios';
import {RideLater} from './index';
import {RideLaterURL} from '../../BaseUrl';



export function RideLaterAction(RideLater) {
    return function (dispatch) {
        axios.post(`${RideLaterURL}`,RideLater)
            .then(function (response) {
                dispatch({type: "RideLater", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "RideLater_REJECT", payload:err})
            })
    }

}
