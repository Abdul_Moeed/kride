"use strict"
import axios from 'axios';
import {ImageUpload} from './index';
import {ImageUploadUrl} from '../../BaseUrl';



export function ImageUploadAction(ImageUpload) {

    return function (dispatch) {
        axios.post(`${ImageUploadUrl}`,ImageUpload)
            .then(function (response) {
                dispatch({type: "ImageUpload", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "ImageUpload_REJECT", payload:err})
            })
    }

}




