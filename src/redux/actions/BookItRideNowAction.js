"use strict"
import axios from 'axios';
import {RideNow} from './index';
import {RideNowURL} from '../../BaseUrl';



export function RideNowAction(RideNow) {
    return function (dispatch) {
        axios.post(`${RideNowURL}`,RideNow)
            .then(function (response) {
                dispatch({type: "RideNow", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "RIDENOW_REJECT", payload:err})
            })
    }

}
