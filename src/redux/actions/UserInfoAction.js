"use strict"
import axios from 'axios';
import {USERINFO} from './index';
import {UserInfoUrl} from '../../BaseUrl';



export function UserInfoAction(userinfo) {

    return function (dispatch) {
        axios.post(`${UserInfoUrl}`,userinfo)
            .then(function (response) {
                dispatch({type: "USERINFO", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "USERINFO_REJECT", payload:err})
            })
    }

}




