"use strict"
import axios from 'axios';
import {RIDESTATUS} from './index';
import {RIDESTATUSURL} from '../../BaseUrl';



export function RideStatusAction(RideStatus) {

    return function (dispatch) {
        axios.post(`${RIDESTATUSURL}`,RideStatus)
            .then(function (response) {
                dispatch({type: "RIDESTATUS", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "RIDESTATUS_REJECT", payload:err})
            })
    }

}




