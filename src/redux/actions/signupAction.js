/**
 * Created by Abdul Moeed Saleem on 22-Sep-17.
 */
"use strict";
import axios from 'axios';
import {SIGNUP} from './index';
import {SignupUrl} from '../../BaseUrl'



export function signupAction(signup) {
    return function (dispatch) {
        axios.post(`${SignupUrl}`, signup)
            .then(function (response) {
                dispatch({type: "SIGNUP", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "SIGNUP_REJECT", payload: err})
            })
    }

}


