import {UPDATEUSER} from './index'
import axios from 'axios'
import {UpdateuserUrl} from '../../BaseUrl'

export function UpdateUserAction(updateuser) {
    return function (dispatch) {
        axios.post(`${UpdateuserUrl}`,updateuser)
            .then(function (response) {
                dispatch({type: "UPDATEUSER", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "UPDATEUSER_REJECT", payload:err})
            })
    }
}