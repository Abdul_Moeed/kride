"use strict"
import axios from 'axios';
import {LOGIN} from './index';
import {LoginUrl} from '../../BaseUrl';



export function loginAction(login) {

    return function (dispatch) {
        axios.post(`${LoginUrl}`,login)
            .then(function (response) {
                dispatch({type: "LOGIN", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "LOGIN_REJECT", payload:err})
            })
    }

}




