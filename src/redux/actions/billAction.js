import axios from 'axios';
import {billFare} from './index';
import {billUrl} from '../../BaseUrl'

export function billAction(amount) {
    return function (dispatch) {
        axios.post(`${billUrl}`, amount)
            .then(function (response) {
                dispatch({type:billFare, payload:response.data})

            })
            .catch(function (err) {
                dispatch({type:"Bill Not Provided", payload:err})
            })
    }

}