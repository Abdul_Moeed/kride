import axios from 'axios';
import {starRating} from './index';
import {ratingUrl} from '../../BaseUrl';

export function ratingAction(sRate) {
    return function (dispatch) {
        axios.post(`${ratingUrl}`, sRate)
            .then(function (response) {
                dispatch({type:starRating, payload:response.data})
            })
            .catch(function (err) {
                dispatch({type:"User Is not Rated", payload:err})

            })

    }

}