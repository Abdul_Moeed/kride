"use strict"
import axios from 'axios';
import {MYBOOKING} from './index';
import {MyBooking} from '../../BaseUrl';



export function myBookingAction(myBooking) {

    return function (dispatch) {
        axios.post(`${MyBooking}`,myBooking)
            .then(function (response) {
                dispatch({type: "MYBOOKING", payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "MYBOOKING_REJECT", payload:err})
            })
    }

}




