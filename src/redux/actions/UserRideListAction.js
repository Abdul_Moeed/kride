"use strict"
import axios from 'axios';
import {userRideList_INFO} from './index';
import {userRideListInfo} from '../../BaseUrl'


export function UserRideListAction(userRideList) {
    return function (dispatch) {
        axios.post(`${userRideListInfo}`, userRideList)
            .then(function (response) {
                dispatch({type: userRideList_INFO, payload: response.data})
            })
            .catch(function (err) {
                dispatch({type: "userRideList_REJECT", payload: err})
            })
    }

}