import {LOGOUT} from './index'

export function logoutAction() {
    return{
        type: "LOGOUT",
        payload: "User is LogOut"
    }
}