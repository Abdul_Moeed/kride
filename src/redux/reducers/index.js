import {combineReducers} from 'redux';
import {loginReducer} from './loginReducer';
import {signupReducer} from './signupReducer';
import {pickupReducer} from './pickupReducer';
import {UserRideListReducer} from './UserRideListReducer';
import {RideNowReducer} from './BookItRideNowReducer';
import {RideLaterReducer} from './BookItRideLaterReducer'
import {UserInfoReducer} from './UserInfoReducer'
import {RideStatusReducer} from './RideStatusReducer'
import {addCreditCardReducer} from './addCreditCardReducer'
import {myBookingReducer} from './myBookingReducer'
import {billReducer} from './billReducer';
import {ratingReducer} from './ratingReducer'
import {UpdateUserReducer} from './UpdateUserReducer'
import {ImageUploadReducer} from './ImageUploadReducer'

const appReducer = combineReducers({
    login:loginReducer,
    signup:signupReducer,
    region:pickupReducer,
    userRideList:UserRideListReducer,
    RideNow:RideNowReducer,
    RideLater:RideLaterReducer,
    userinfo:UserInfoReducer,
    RideStatus:RideStatusReducer,
    addCreditCard:addCreditCardReducer,
    myBooking:myBookingReducer,
    amount:billReducer,
    sRate:ratingReducer,
    updateuser:UpdateUserReducer,
    ImageUpload:ImageUploadReducer
})


const Reducers = (state, action) => {
    if (action.type === 'LOGOUT') {
        state = undefined
    }

    return appReducer(state, action)
}

export default Reducers