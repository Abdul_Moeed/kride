/**
 * Created by Abdul Moeed Saleem on 22-Sep-17.
 */
import {SIGNUP} from '../actions/index';

export function signupReducer(state={signup:[]},action) {
    switch (action.type){
        case "SIGNUP":
            return {signup: [...state, action.payload]};
            break;
    }
    return state;
}