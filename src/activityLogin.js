import React, {Component} from 'react';
import {ActivityIndicator, View,StyleSheet, AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';


class activityLogin extends Component {
    constructor(props){
        super(props);
    this.state = { animating: true }

    }
    closeActivityIndicator=()=>setTimeout(()=>this.setState({animating: false }),6000)

    //routes
    goToLogin(){
        Actions.landing()
    }
    goToHome(){
        Actions.appmenu()
    }

    // onEnableLocationPress() {
        
    //   }

    componentDidMount(){

        this.closeActivityIndicator()

    }

    componentDidMount(){
        AsyncStorage.getItem("_id").then((userId)=>{
            if (userId !== null) {
                return this.goToHome()
            }
            else{
                return this.goToLogin()
            }
        })
    }

    render() {
        const animating = this.state.animating
        return (
            <View style = {styles.container}>
                <ActivityIndicator
                    animating = {animating}
                    color = '#063E6A'
                    size = "large"
                    style = {styles.activityIndicator}/>
            </View>
        )
    }
}


const styles = StyleSheet.create ({
    container: {
        marginTop:22,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //  marginTop: 70
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80
    }
})

export default activityLogin
