import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    AsyncStorage
} from 'react-native';
import RideStartMap from './MapClasses/RideStartMap';
import {connect} from 'react-redux';



class RunningRides extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount(){
        this.ServerResponse()
    }

    ServerResponse(){
        this.Interval = setInterval(()=> {
            this.props.RideStatus.map((x) => {
                AsyncStorage.setItem('RIDESTATUS', x.RideStatus)
                if(x.RideStatus === 5){
                    Actions.billing()
                }
            })
        },3000)
    }

    render() {
        return(
            <View style={styles.main}>
                <RideStartMap/>
            </View>

        )
    }
}
const styles = StyleSheet.create({

    main:{
        // marginTop:22,
        flex: 1
    }
});

function mapStateToProps(state) {
    return{
        RideStatus:state.RideStatus.RideStatus
    }
}


export default connect (mapStateToProps, null)(RunningRides)