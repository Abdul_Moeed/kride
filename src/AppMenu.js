import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    BackHandler,
    AsyncStorage,
    Modal,
    TextInput,
    TouchableHighlight,
    ToastAndroid,
    PermissionsAndroid,
    Picker,
} from 'react-native';
import Menu from './Menu';
import Mapclass from './MapClasses/PickupMapclass';
import { Actions} from 'react-native-router-flux';
import DrawerLayout from 'react-native-drawer-layout'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import DatePicker from 'react-native-datepicker';
// import {UserInfoAction} from './redux/actions/UserInfoAction'


const image = require('./ImagesSources/menu.png');


const styles = StyleSheet.create({

    buttons:{
        flexDirection: 'row',
      //  justifyContent: 'flex-end',
     //   alignItems: 'center',


    },

    bStyle:{
        backgroundColor:'#023c68',
        borderColor: '#F9A402',
        borderWidth: 2,
        width: '50%',
        height:50,
        alignItems: 'center',
        justifyContent: 'center',



    } ,

    bStyle1:{
        backgroundColor:'#023c68',
        borderColor: '#F9A402',
        borderWidth: 2,
        width: '100%',
        height:50,
        alignItems: 'center',
        justifyContent: 'center',


    } ,
    fonts:{
        color:'white',
    },

    buttonimage:{
      flexDirection: 'row'

    },

    basicbutton:{
        borderColor: 'black',
        borderWidth: 1,
        width: '50%',
        height:75,
        alignItems: 'center',
        justifyContent: 'center',



    } ,

    premiumbutton:{
        borderColor: 'black',
        borderWidth: 1,
        width: '50%',
        height:75,
        alignItems: 'center',
        justifyContent: 'center',


    } ,

    basic:{
      height: '100%',
        width: '100%',

    },

    premium:{
        height: '100%',
        width: '100%',

    },

    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,

        flexDirection: 'row',
    },



    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
       margin:10,



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },


    button: {
        justifyContent: 'center',
        marginLeft:10,
        marginRight: 5,

    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    container: {
      //  marginTop:22,
        flex: 1,
    },

});


class AppMenu extends Component {

    constructor(props) {
        super(props);
        this.state={
            ServiceType:'basic',
            text:'PICK UP LOCATION',
            selectedValue:'',
            pickupTime:'',
        }
    }


    

    componentDidMount(){

        var date = String(new Date().getDate())
        var monthI = String(new Date().getMonth() + 1)
        var monthF = String(new Date().getMonth() + 2)
        var year = String(new Date().getFullYear())
        initialDate = year + '-' + monthI + '-' + date
        finalDate = year + '-' + monthF + '-' + date
          
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        // this.Userinfo()

        AsyncStorage.getItem('RIDESTATUS').then((item)=>{
            let rideState = Number(item)
            if(rideState === 1){
                return Actions.ridestart()
            }
            if(rideState === 2){
                return Actions.ridestart()
            }
            if(rideState === 3){
                return Actions.ridestart()
            }
            if(rideState === 4){
                return Actions.ridestart()
            }
             if(rideState === 5){
                return Actions.billing()
            }
        })
        this.BasicServiceType()

    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }



    handleBackButton(){
        ToastAndroid.show('For Exit Press Home Button', ToastAndroid.SHORT);
        return  true;
     }


    BasicServiceType(){
        let Basic = "basic"
        AsyncStorage.setItem("ServiceType", Basic)
    }

    renderButtonView(){
        if (this.state.ServiceType === 'basic'){
            return (
                <View style={styles.buttons}>
                <TouchableHighlight onPress = {()=>Actions.ridenow()}  style={styles.bStyle}>
                            <Text style = {styles.fonts}> Ride Now </Text>
                        </TouchableHighlight>
                        <TouchableHighlight onPress = {()=>Actions.ridelater()} style={styles.bStyle}>
                            <Text style = {styles.fonts}> Ride Later </Text>
                        </TouchableHighlight>
                        </View>
            )
        }
        else{
            return(
                <View stye={styles.buttons}>
                <TouchableHighlight style={styles.bStyle1}>
                    <Text style={styles.fonts}>Continue!</Text>
                </TouchableHighlight>
                </View>
            )
        }
    }

    renderHeadView(){
        if(this.state.ServiceType === 'basic'){
          return (
            <View style={styles.headtextview} >
            <Text style={styles.headtext}>PICK UP LOCATION</Text>
        </View>
          )
        }
        else {
            return(
                <View style={styles.headtextview} >
                            <Text style={styles.headtext}>PLEASE FILL THE FORM</Text>
                        </View>
            )
        }
    }

    renderBodyView(){
        if(this.state.ServiceType === 'basic'){
            return(
                <Mapclass/>
            )
        }
        if(this.state.ServiceType === 'coaster'){
            return(
                <View style={{height:490, width:'100%', backgroundColor:'pink'}}>
                <ScrollView>
                <TextInput
                style={styles.input}
                placeholder='Name'
                placeholderTextColor='silver'
                autoCapitalize='words'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.second.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='second'
                placeholder='Phone Number'
                placeholderTextColor='silver'
                keyboardType='phone-pad'
                autoCapitalize=''
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.third.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='third'
                placeholder='Pick Up Location'
                placeholderTextColor='silver'
                autoCapitalize='none'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.forth.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='forth'
                placeholder='Drop Off Location'
                placeholderTextColor='silver'
                autoCapitalize='none'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.fifth.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='fifth'
                placeholder='No of Persons'
                placeholderTextColor='silver'
                keyboardType='number-pad'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.sixth.focus()
                }}
                />
                <DatePicker
                                style={{width: '80%', margin:5}}
                                date={this.state.pickupTime}
                                mode="datetime"
                                placeholder="Select Pickup Time and Date"
                                format="YYYY-MM-DD  HH:MM:SS"
                                minDate={initialDate}
                                maxDate={finalDate}
                                confirmBtnText="Ok"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,

                                    },
                                    dateInput: {

                                        alignItems: 'center'

                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({pickupTime: date})}}
                            />                            
                
                <TextInput
                style={styles.input}
                refs='sixth'
                placeholder='Pick up Time and Date'
                placeholderTextColor='silver'
                autoCapitalize=''
                // onChangeText={}
                returnKeyType=''
                onSubmitEditing={(event)=>{
                    this.refs.second.focus()
                }}
                />
                </ScrollView>
                </View>
            )
        }

            else{
            return(
                <View style={{height:490, width:'100%', backgroundColor:'#00bfff'}}>
                <ScrollView>
                <TextInput
                style={styles.input}
                placeholder='Name'
                placeholderTextColor='gray'
                autoCapitalize='words'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.second.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='second'
                placeholder='Phone Number'
                placeholderTextColor='silver'
                keyboardType='phone-pad'
                autoCapitalize=''
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.third.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='third'
                placeholder='Pick Up Location'
                placeholderTextColor='silver'
                autoCapitalize='none'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.forth.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='forth'
                placeholder='Drop Off Location'
                placeholderTextColor='silver'
                autoCapitalize='none'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.fifth.focus()
                }}
                />
                <TextInput
                style={styles.input}
                refs='fifth'
                placeholder='No of Persons'
                placeholderTextColor='silver'
                keyboardType='number-pad'
                // onChangeText={}
                returnKeyType='next'
                onSubmitEditing={(event)=>{
                    this.refs.sixth.focus()
                }}
                />
                <DatePicker
                                style={{width: '80%', margin:5}}
                                date={this.state.pickupTime}
                                mode="datetime"
                                placeholder="Select Pickup Time and Date"
                                format="YYYY-MM-DD  HH:MM:SS"
                                minDate={initialDate}
                                maxDate={finalDate}
                                confirmBtnText="Ok"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,

                                    },
                                    dateInput: {

                                        alignItems: 'center'

                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({pickupTime: date})}}
                            />                            
                
                <TextInput
                style={styles.input}
                refs='sixth'
                placeholder='Pick up Time and Date'
                placeholderTextColor='silver'
                autoCapitalize=''
                // onChangeText={}
                returnKeyType=''
                onSubmitEditing={(event)=>{
                    this.refs.second.focus()
                }}
                />
                </ScrollView>
                </View>
            )
        }
    }



    render() {

        const goToRideNow = () => {
                   Actions.ridenow()

               }

        const goToAppMenu = () => {
            Actions.appmenu()

        }
               const goToRideLater = () => {
                   Actions.ridelater()

               }

        const menu = <Menu/>;



        return (

            <DrawerLayout
            drawerWidth={220}
            ref={(drawer) => { return this.drawer = drawer }}
            keyboardDismissMode="on-drag"
            renderNavigationView={() => menu}>

                        <View style={styles.container}>

                    <View style={styles.headbar}>

                            <View style={styles.button}>
                                <TouchableHighlight onPress={() => this.drawer.openDrawer()}>
                                    <Image source={require('./ImagesSources/menu.png')} style={{ width: 32, height: 32}} />
                                </TouchableHighlight>
                            </View>

                        {this.renderHeadView()}


                    </View>
                            {/*<View style={{borderWidth:2, borderColor:'black',justifyContent:'center', height:50}}>*/}
                                {/*{this.state.FormatedAddress === null ? <Text onPress={() => Actions.GooglePlacesPickup()}>Pickup Location</Text>:*/}
                                    {/*<Text onPress={() => Actions.GooglePlacesPickup()}>{this.state.FormatedAddress}</Text>}*/}
                            {/*</View>*/}




                        {this.renderBodyView()}





                    <View style={styles.buttonimage}>
                        {/* <TouchableHighlight style={styles.basicbutton} onPress={()=> this.BasicServiceType()}>
                            <Image style={styles.basic} source = {require('./ImagesSources/basic.png')}/>
                        </TouchableHighlight>
                        <TouchableHighlight  style={styles.premiumbutton} onPress={()=> this.PremiumServiceType()}>
                            <Image style={styles.premium} source = {require('./ImagesSources/premium.png')}/>
                        </TouchableHighlight> */}

                        <Picker
                        selectedValue={this.state.ServiceType}
                        onValueChange={(itemValue, itemIndex)=> this.setState({ServiceType: itemValue}) || AsyncStorage.setItem('ServiceType', this.state.ServiceType)}
                        style={{width:'100%', height:70}}
                         >
                            <Picker.Item label = 'Book a car' value = 'basic' />
                            <Picker.Item label = 'Van/Coaster' value = 'coaster' />
                            <Picker.Item label = 'Luggage Through Road' value = 'road' />
                            <Picker.Item label = 'Luggage Through Air' value = 'air' />
                            <Picker.Item label = 'Luggage Through Sea' value = 'sea' />
                            </Picker>                        


                    </View>



                    {this.renderButtonView()}
                </View>
        </DrawerLayout>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        // UserInfoAction,
    },dispatch)
}

function mapStateToProps(state) {
    return{
        userinfo:state.userinfo.userinfo
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(AppMenu)