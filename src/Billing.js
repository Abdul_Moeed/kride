import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, TouchableHighlight,Text, Image, TextInput, ScrollView, AsyncStorage, BackHandler, ImageBackground } from 'react-native';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import PopupDialog, {DialogTitle} from 'react-native-popup-dialog'
import {billAction} from './redux/actions/billAction'
import {ratingAction} from './redux/actions/ratingAction'
import  Rating from 'react-native-easy-rating'
import  {logoutAction} from './redux/actions/logoutAction'


class Billing extends Component{

    constructor(){
        super();
        this.state={
            rideid:"",
            rating:0,
            date: new Date().toLocaleDateString(),
            time: new Date().toLocaleTimeString(),
            amountRecieved:null
        }
    }



    componentDidMount(){
        AsyncStorage.getItem('RIDEID').then((item)=>{
            this.setState({rideid:item})
            let RID ={
                rideid:item
            }
            this.props.billAction(RID)
            // console.log('bill wala data', RID)
        })
    }

    rateUser(){
        let ratingdata ={
            rideid:this.state.rideid,
            captainRating:this.state.rating
        }
        this.props.ratingAction(ratingdata)
        // console.log('Rating Wala Data', ratingdata)
    }


    // rideStatus(){
    //     let status = String(6)
    //     AsyncStorage.setItem('RIDESTATUS', status)
    // }


    UserRating(){
return(
    <View style={{alignItems:'center', justifyContent:'center',}}>


        <Rating style ={{marginTop:35}}
            rating={0}
            max={5}
            iconWidth={50}
            iconHeight={50}
            iconSelected={require('./ImagesSources/rateStar.png')}
            iconUnselected={require('./ImagesSources/unrateStar.png')}
            onRate={(rating) => this.setState({rating: rating})}/>

        <Text onPress={()=>{
         //   this.rideStatus()
            this.rateUser()
            this.removigUsedData()
         //   BackHandler.exitApp()
            Actions.appmenu()
            this.props.logoutAction()
        }} style={{
            height:50,
            width:150,
            backgroundColor:'#023c68',
            textAlign:'center',
            borderRadius:8,
            marginTop:50,
            paddingTop:10,
            color:'white',
            fontSize:20,
            fontWeight:'bold'
        }}> Rate Driver </Text>
    </View>
)


    }

removigUsedData(){

        AsyncStorage.removeItem("RIDEID");
        AsyncStorage.removeItem("PickupLatitude");
        AsyncStorage.removeItem("PickupLongitude");
        AsyncStorage.removeItem("DropoffLatitude");
        AsyncStorage.removeItem("DropoffLongitude");
        AsyncStorage.removeItem('RIDESTATUS');
        AsyncStorage.removeItem('ServiceType')
    AsyncStorage.removeItem('FormatedAddressdropoff')

    AsyncStorage.removeItem('FormatedAddress')



}

  render(){
    return(
      <View style={styles.main}>
              <View style={styles.headbar}>


                  <View style={styles.headtextview} >
                      <Text style={styles.headtext}>BILL</Text>
                  </View>
                  </View>
          <PopupDialog
              dialogTitle = {<DialogTitle title = "Rate Driver"/>}
              ref={(popupDialog) => { this.cashDialog = popupDialog; }}
          >
              {this.UserRating()}
          </PopupDialog>
                    <Image style={styles.backgroundImage} source = {require('./ImagesSources/background.png')}>


                        {this.props.amount.map((x)=>{
                            // console.log('BILL WALA AMOUNT', x)
                            return(
                  <View style={styles.billView}>
                    <View style={styles.billviewimage}>
                    <View style={styles.billviewimage1}>
                    <View style={styles.dateviewimage}>
                    <Text style={styles.dateviewtext}>
                        Date: {this.state.date} {'   '}Time: {this.state.time}
                    </Text>
                    </View>
                    <View style={styles.direction}>
                    <View style={styles.costingview}>
                    <Text style={styles.costingviewtext}>
                    Base Price
                    </Text>
                    <Text style={styles.costingviewtext}>
                    Cost / KM
                    </Text>
                    <Text style={styles.costingviewtext}>
                    Cost / Waiting
                    </Text>
                    <Text style={styles.costingviewtext}>
                    Total Amount
                    </Text>
                    </View>
                    <View style={styles.rateview}>
                    <Text style={styles.rateviewtext}>
                    Rs:{x.base}
                    </Text>
                    <Text style={styles.rateviewtext}>
                    Rs:{x.km}
                    </Text>
                    <Text style={styles.rateviewtext}>
                    Rs:{x.permint}
                    </Text>
                    <Text style={styles.rateviewtext}>
                    Rs:{x.Fair}
                    </Text>
                    </View>
                    </View>
                    <View style={styles.downtextview}>
                    <Text style={styles.downviewtext}>
                        Ride ID = {this.state.rideid}
                    </Text>
                    <Text style={styles.downviewtext}>
                    Thank you for your business
                    </Text>
                    </View>
                    </View>
                    </View>
                  </View>
                            )
                        })

                        }

                  <View style={styles.bill}>

                  
                  </View>

                  <View style={styles.buttons}>
                  <TouchableOpacity style={styles.bStyle} onPress={()=>{
                      this.cashDialog.show();
                  }}>
                      <Text style = {styles.fonts}> DONE </Text>
                  </TouchableOpacity>
                </View>
              </Image>
                </View>


    );
  }
};

const styles = StyleSheet.create({
  main:{
    // marginTop:22,
    flex: 1,
    width: View.width,
    height: View.height,

  },
   backgroundImage:{
   flex: 1,
       resizeMode: 'cover',
       width: View.width,
       height: View.height,
       justifyContent: 'flex-end',

   },
    headbar:{
      backgroundColor:'#032f53',
        width: View.width,
        height: '10%',
        flexDirection: 'row',
    },
    headtextview:{
        justifyContent: 'center',
        marginLeft:'10%'
    },
    headtext:{
        color:'white',
        fontSize: 20,
    },
    backiconview:{
        justifyContent: 'center',
    },
    buttons:{
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
   bStyle:{
       backgroundColor:'#023c68',
       borderColor: '#F9A402',
       borderWidth: 2,
       width: '100%',
       height:50,
       alignItems: 'center',
       justifyContent: 'center',
   } ,
    fonts:{
       color:'white',
    },
    billView:{
    height: '67%',
      width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    bill:{
      height: '20%',
      flexDirection: 'row',
    },
    billtext:{
      fontSize:15,
      color: 'white',
      margin: 1,
      marginLeft: '30%'
    },
    billtextinput:{
      width: '60%',
      height:30,
    borderColor: '#FFA500',
    borderWidth: 2,
    margin: 2,
    backgroundColor: 'white',
    paddingTop: '2%',
    paddingLeft: '2%',
    color: 'black',
    fontWeight: '500',
        height:View.height
    },
    billtextview:{
      width: '40%',
        justifyContent: 'center',
    },
    billtextinputview:{
        justifyContent: 'center',
        alignItems: 'center',
      width: '60%',

    },
    billviewimage:{
      width: '95%',
      height: 300,
      justifyContent: 'flex-start',
      alignItems: 'center',
        backgroundColor:'#ffffff',
        borderRadius:8

    },
    billviewimage1:{
      height: View.height,
      width: '90%',

    },
    dateviewimage:{
      height: '10%',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'flex-end'
    },
    dateviewtext:{
      color: 'black',
      marginRight: '5%',
      fontWeight: '500',
    },
    direction:{
      flexDirection: 'row',
      height: '60%',
        justifyContent: 'center',
    },
    costingview:{
      height: '100%',
      width: '50%',
      justifyContent: 'center',
      alignItems: 'flex-start',

    },
    rateview:{
      height: '100%',
        width: '50%',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    costingviewtext:{
      color: 'black',
      fontWeight: '500',
      margin: 5,
      marginLeft: 10
    },
    rateviewtext:{
      color: 'black',
      fontWeight: '500',
      margin: 5,
        marginRight: 10
    },
    downtextview:{
      height: '27%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    downviewtext:{
      color: 'black',
      fontWeight: '500',
    },


});

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        billAction,
        ratingAction,
        logoutAction
    },dispatch)
}

function mapStateToProps(state) {
    return{
        amount:state.amount.amount
    }
}


export default connect(mapStateToProps, mapDispatchToProps) (Billing)