import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    BackHandler,
    Alert,
    Modal,
    ActivityIndicator,
    TouchableHighlight,
    AsyncStorage,
    ToastAndroid
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import Mapclass from "./MapClasses/DropoffMapclass";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {RideNowAction} from './redux/actions/BookItRideNowAction';
import PopupDialog  from 'react-native-popup-dialog'
import RNGooglePlaces from 'react-native-google-places';

const image = require('./ImagesSources/back.png');

const styles = StyleSheet.create({
    bottomText:{
        backgroundColor: 'transparent',
        height: '7%',
        justifyContent: 'center',
        alignItems: 'center'
    },

    buttons:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    bStyle:{
        backgroundColor:'#023c68',
        borderColor: '#F9A402',
        borderWidth: 2,
        width: '100%',
        height:50,
        alignItems: 'center',
        justifyContent: 'center',
    } ,

    fonts:{
        color:'white',
    },

    Cashbutton:{
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor:'#6789A5',
        width: '50%',
        height:60,
        alignItems: 'center',
        justifyContent: 'center',
    },

    Dropoffbutton:{
        borderColor: 'black',
        borderWidth: 1,
        backgroundColor:'#6789A5',
        width: '50%',
        height:60,
        alignItems: 'center',
        justifyContent: 'center',
    },

    Cash:{
        height: '100%',
        width: '100%',
    },

    Estimate:{
        height: '100%',
        width: '100%',
    },

    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,
        flexDirection: 'row',
    },

    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,
    },

    headtext:{
        color:'white',
        fontSize: 20,
    },

    button: {
        justifyContent: 'center'
    },

    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },

    container: {
        // marginTop:22,
        flex: 1,
    },
});

class RideNow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _id: '',
            pickupLatitude: null,
            pickupLongitude: null,
            dropoffLatitude: null,
            dropoffLongitude: null,
            paymentMethod: '',
            phoneNumber: '',
            serviceType:'',
            FormatedAddress:null,
            userName:'',
        }
    }

    handleBookIt(){
            const RideNow = {
                userid: this.state._id,
                pickupLatitude: this.state.pickupLatitude,
                pickupLongitude: this.state.pickupLongitude,
                dropoffLatitude: this.state.dropoffLatitude,
                dropoffLongitude: this.state.dropoffLongitude,
                paymentMethod: this.state.paymentMethod,
                userPhoneNumber: this.state.phoneNumber,
                servicetype:this.state.serviceType,
                username: this.state.userName
    
            }
    
       this.props.RideNowAction(RideNow)
    
        console.log('ACTION TRIGER:', RideNow)
    }


    Check(){
        AsyncStorage.getItem("_id").then((_id) => {
            this.setState({_id:_id})
            console.log(_id)
        })

            AsyncStorage.getItem("pickupLatitude").then((xyz) => {
                let pickupLatitude = Number(xyz)
                this.setState({pickupLatitude: pickupLatitude})
                console.log("pickupLatitude", pickupLatitude)
            })

        AsyncStorage.getItem("pickupLongitude").then((xyz) => {
            let pickupLongitude = Number(xyz)
            this.setState({pickupLongitude:pickupLongitude})
            console.log("pickupLongitude",pickupLongitude)
        })

        AsyncStorage.getItem("phoneNumber").then((xyz) => {
            this.setState({phoneNumber:xyz})
            console.log(xyz)
        })

        AsyncStorage.getItem("userName").then((xyz) => {
            this.setState({userName:xyz})
            console.log(xyz)
        })

    }


    DropOffLocation(){
        
        AsyncStorage.getItem("dropoffLatitudeRideNow").then((xyz) => {
            let dropoffLatitude = Number(xyz)
            this.setState({dropoffLatitude:dropoffLatitude})
            console.log("dropoffLatitudeRideNow",dropoffLatitude)


            AsyncStorage.getItem("dropoffLongitudeRideNow").then((xyz) => {
                let dropoffLongitude = Number(xyz)
                this.setState({dropoffLongitude:dropoffLongitude})
                console.log("dropoffLongitudeRideNow",dropoffLongitude)

                this.handleBookIt()
            })

        })

    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.Check()

        if(!this.state.paymentMethod) {
            let Cash = 'Cash'
            AsyncStorage.setItem("PaymentType", Cash)
            console.log('setCASH',Cash)
        }

        AsyncStorage.getItem("PaymentType").then((Cash) => {
            this.setState({paymentMethod:Cash})
            console.log('getCash',Cash)
        })

        if(this.state.serviceType == '') {
             AsyncStorage.getItem("ServiceType").then((abc) => {
                this.setState({serviceType:abc})
                console.log(abc)
            })
        }


    }


    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }


    handleBackButton(){

        ToastAndroid.show('For Back Press top Left Button', ToastAndroid.LONG);
        return true;
    }

    handleSMS(){
        alert("SMS Driver")
    }


    handleCALL(){
        alert("CALL Driver")
    }

    handleTiger(){
        this.DropOffLocation();
    }

    saveCash(){
        return(
            AsyncStorage.getItem("PaymentType").then((Cash) => {
                this.setState({paymentMethod:Cash})
                console.log('getCash',Cash)
            })
        )
    }

    DropoffEstimate(){
        return(
            <View style={{ marginTop:"5%", alignItems:'center'}}>
            {/* <View style={{alignItems:'center',width:"90%"}}>
                <Text style = {{fontWeight:'500', fontSize:20, color:'white'}}>PICKUP LOCATION:</Text>
                <Text style = {{fontSize:15, textAlign:'center'}}> CIBA Consulting Pakistan DHA-6 Karachi </Text>
            </View>
            <View style={{alignItems:'center',width:"90%",marginTop:'5%'}}>
    <Text style = {{fontWeight:'500', fontSize:20, color:'white'}}>DROPOFF LOCATION:</Text>
                <Text style = {{fontSize:15, textAlign:'center'}}> Garden Zoo Karachi </Text>
        </View> */}
                <View style={{alignItems:'center',width:"90%",marginTop:'5%'}}>
                    <Text style = {{fontWeight:'500', fontSize:20, color:'white'}}>ESTIMATE:</Text>
                    <Text style = {{color:'black', fontWeight:'bold', fontSize:30, textDecorationLine:'underline' }}> Sorry! This service is not available right now </Text>
                </View>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <TouchableHighlight onPress={()=> this.dropoffDialog.dismiss()}
                        style={{height:50,width:100,margin:10,alignItems:'center', justifyContent:'center',borderRadius:10,backgroundColor:'#032f53' }}>

                        <Text style = {{color:'white', fontSize:25, textAlign:'center', fontWeight:'400'}}>OK!</Text>
                    </TouchableHighlight>
                </View>
            </View>

        )
    }



    Cash(){
        return(
            <View style={{ marginTop:"5%", alignItems:'center'}}>
                <View style={{alignItems:'center',width:"90%"}}>
                    <Text style = {{fontWeight:'500', fontSize:20, color:'white'}}>Through Cash</Text>
                </View>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <TouchableHighlight onPress={()=> this.saveCash() && this.cashDialog.dismiss()}
                        style={{height:50,width:100,margin:10,alignItems:'center', justifyContent:'center',borderRadius:10,backgroundColor:'#032f53' }}>

                        <Text style = {{color:'white', fontSize:25, textAlign:'center', fontWeight:'400'}}>Cash</Text>
                    </TouchableHighlight>
                </View>

                <View style={{alignItems:'center',alignItems:'center',width:"90%"}}>
                    <Text style = {{fontWeight:'500', fontSize:20, color:'white'}}>Through Credit Balance</Text>
                </View>
                <View style={{alignItems:'center', justifyContent:'center'}}>
                    <TouchableHighlight onPress={()=> alert('Kindly use Cash for Now')}
                        style={{height:50,width:200,margin:10,alignItems:'center', justifyContent:'center',borderRadius:10,backgroundColor:'#032f53' }}>
                        <Text style = {{color:'white', fontSize:25, textAlign:'center', fontWeight:'400'}}>Credit Balance</Text>
                    </TouchableHighlight>
                </View>

            </View>
        )
    }

    openSearchModal() {
        RNGooglePlaces.openPlacePickerModal()
            .then((place) => {
                console.log(place);

                console.log( "latitude",place.latitude,
                    "longitude",place.longitude,
                    "placeAddress", place.address)

                AsyncStorage.setItem("PlaceAddress", place.address)
                // place represents user's selection from the
                // suggestions and it is a simplified Google Place object.
            })
            .catch(error => console.log(error.message));  // error is a Javascript Error object
    }


    render() {

        const goToIndicator = () => {
            Actions.indicator()
        }

        const goToMenu = () => {
            Actions.appmenu()
        }

        AsyncStorage.getItem('PlaceAddress').then((x) => {
            this.setState({FormatedAddress:x})
        })

        return (

                <View style={styles.container}>

                    <View style={styles.headbar}>

                        <TouchableHighlight
                            onPress = {goToMenu}
                            style={styles.button}
                        >
                            <Image
                                source={image}
                                style={{ width: 32, height: 32}}
                            />
                        </TouchableHighlight>

                        <View style={styles.headtextview} >
                            <Text style={styles.headtext}>BOOK RIDE</Text>
                        </View>


                    </View>

                    

                        <Mapclass/>

                    <PopupDialog
                        ref={(popupDialog) => { this.cashDialog = popupDialog; }}
                    >
                        {this.Cash()}
                    </PopupDialog>


                    <PopupDialog
                        ref={(popupDialog) => { this.dropoffDialog = popupDialog; }}
                    >
                        {this.DropoffEstimate()}
                    </PopupDialog>

{/* 
                    <View style={styles.bottomText} >

                        <Text style={styles.fonts}>
                            EXTRA REQUIREMENTS BOX
                        </Text>

                    </View> */}

                    <View style={styles.buttons}>
                        <TouchableHighlight  style={styles.Cashbutton}
                                             onPress={() => {
                                                 this.cashDialog.show();
                                             }}>
                            <Text style = {{color:'black', fontWeight:'300', fontSize:20}}>CASH</Text>
                        </TouchableHighlight>

                        <TouchableHighlight  style={styles.Dropoffbutton}
                                             onPress={() =>{
                                                 this.dropoffDialog.show();
                                             }}>
                            <Text style = {{color:'black', fontWeight:'300', fontSize:20, textAlign:'center'}}>Dropoff{'\n'}Estimate</Text>
                        </TouchableHighlight>

                    </View>

                    <View style={styles.buttons}>
                        <TouchableHighlight onPress={ () => {
                            this.handleTiger()
                            goToIndicator()

                        }}

                                            style={styles.bStyle}>
                            <Text style = {styles.fonts} > BOOK IT </Text>
                        </TouchableHighlight>
                    </View>







                </View>


        );
    }
}


function mapStateToProps(state) {
    return{

        RideNow:state.RideNow.RideNow
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

        RideNowAction

    }, dispatch)
}


export default connect (mapStateToProps, mapDispatchToProps)(RideNow)