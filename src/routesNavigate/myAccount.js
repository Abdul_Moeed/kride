import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    BackHandler,
    TextInput,
    Image,
    AsyncStorage,
    View,
    ScrollView,
    KeyboardAvoidingView,
    TouchableHighlight
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {UpdateUserAction} from '../redux/actions/UpdateUserAction'
import {ImageUploadAction} from '../redux/actions/ImageUploadAction'


// import ImagePicker from 'react-native-image-picker';

class myAccount extends Component {

    constructor(props){
        super(props)
        this.state = {
            userid:'',
            userName:'',
            email:'',
            phoneNumber:'',
            password:'',
            // avatarSource: null,
            // Image:null
        }
    }


    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    validatePassword = (password) =>{
        var re = /^.{6,}/
        return re.test(password)
    };
    validatePhone = (phoneNumber) => {
        var re = /^.{9,}/
        return re.test(phoneNumber)
    };
    validateName = (userName) =>{
        var re =  /^[a-zA-Z ]{2,30}$/
        return re.test(userName)
    }

    handleUpdateUser(){
        const updateuser = {
            userid:this.state.userid,
            userName:this.state.userName,
            email:this.state.email,
            phoneNumber:this.state.phoneNumber,
            password:this.state.password,

        }
        // const ImageUpload = {
        //     userid:this.state.userid,
        //     sampleFile:this.state.Image
        // }

        this.props.UpdateUserAction(updateuser)
        // this.props.ImageUploadAction(ImageUpload)
        // console.log(updateuser)
        // console.log(ImageUpload)
    }

    // selectPhotoTapped() {
    //     const options = {
    //         quality: 1.0,
    //         maxWidth: 500,
    //         maxHeight: 500,
    //         storageOptions: {
    //             skipBackup: true
    //         }
    //     };

    //     ImagePicker.launchImageLibrary(options, (response) => {
    //         console.log('Response = ', response);

    //         let image = response.uri

    //         let source = { uri: response.uri };

    //         AsyncStorage.setItem('image', response.uri)

    //         this.setState({
    //             avatarSource: source,
    //             Image:image
    //         });

    //     });
    // }

    goToAppMenu(){
        Actions.appmenu()
    }

    componentDidMount(){
        this.UserDetail()

        AsyncStorage.getItem('_id').then((x) => {
            this.setState({userid:x})
            // console.log(x)
        })

        // AsyncStorage.getItem('image').then((x) => {
        //     let source = {uri: x}
        //     this.setState({avatarSource:source})
        //     console.log(x)
        // })
    }

    UserDetail(){
        // return this.props.userinfo.map((x) => {
        //     this.setState({
        //         userName:x.userName,
        //         email:x.email,
        //         phoneNumber:x.phoneNumber,
        //         password:x.password
        //     })
        // })
        AsyncStorage.getItem("userName").then((x)=>{
            this.setState({userName:x})
        })
        AsyncStorage.getItem("email").then((x)=>{
            this.setState({email:x})
        })
        AsyncStorage.getItem("phoneNumber").then((x)=>{
            this.setState({phoneNumber:x})
        })
        AsyncStorage.getItem("password").then((x)=>{
            this.setState({password:x})
        })



    }

    render() {

        const goToBack = () => {
            Actions.appmenu()

        }


        return (
            <View style={styles.main}>

                <View style={styles.headbar}>
                    <View style={styles.backiconview}>
                        <TouchableHighlight onPress = {goToBack}>
                            <Image style={styles.backicon} source = {require('../ImagesSources/back.png')}/>
                        </TouchableHighlight>
                    </View>

                    <View style={styles.headtextview} >
                        <Text style={styles.headtext}>MY ACCOUNT</Text>
                    </View>
                </View>


                <KeyboardAvoidingView style={styles.form}>

                    <View style={styles.userimageView}>
                    <Image style ={{height:100,width:100,borderRadius:50}} source = {require('../ImagesSources/userIcon.png')}/>
                        {/* { this.state.avatarSource === null ? <Text>Select a Photo</Text> : */}
                            
                        {/* } */}

                    </View>
                    {/* <View style={styles.userTextView}>
                        <TouchableHighlight onPress={this.selectPhotoTapped.bind(this)}>
                            <Text style={{fontSize:20}}>Add Image</Text>
                        </TouchableHighlight>
                    </View> */}

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('../ImagesSources/user.png')}/>
                        <TextInput style={styles.Inputtext}
                                   placeholder={this.state.userName}
                                   placeholderTextColor="silver"
                                   autoCapitalize="words"
                                   returnKeyType = {"next"}
                                   onSubmitEditing={(event) => {
                                       this.refs.SecondInput.focus();}}
                                   onChangeText={(text) => this.setState({userName:text})}
                        />

                    </View>

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('../ImagesSources/email.png')}/>
                        <TextInput style={styles.Inputtext}
                                   placeholder={this.state.email}
                                   placeholderTextColor="silver"
                                   autoCapitalize="none"
                                   keyboardType="email-address"
                                   ref="SecondInput"
                                   returnKeyType = {"next"}
                                   onSubmitEditing={(event) => {
                                       this.refs.ThirdInput.focus();
                                   }}
                                   onChangeText={(text) => this.setState({email:text})}
                        />
                    </View>

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('../ImagesSources/phone.png')}/>
                        <TextInput style={styles.Inputtext}
                                   placeholder={this.state.phoneNumber}
                                   placeholderTextColor="silver"
                                   autoCapitalize="none"
                                   keyboardType="phone-pad"
                                   ref="ThirdInput"
                                   returnKeyType = {"next"}
                                   onSubmitEditing={(event) => {
                                       this.refs.ForthInput.focus();
                                   }}
                                   onChangeText={(text) => this.setState({phoneNumber:text})}
                        />
                    </View>

                    <View style={styles.username}>
                        <Image style={styles.icon} source = {require('../ImagesSources/password.png')}/>
                        <TextInput style={styles.Inputtext}
                                   placeholder="Input New Password"
                                   placeholderTextColor="silver"
                                   autoCapitalize="none"
                            //  secureTextEntry={true}
                                   ref="ForthInput"
                                   returnKeyType = {"next"}
                                   // onSubmitEditing={(event) => {
                                   //     this.refs.FifthInput.focus();
                                   // }}
                                   onChangeText={(text) => this.setState({password:text})}
                        />
                    </View>


                    <TouchableHighlight style = {styles.submitbutton} onPress={()=> {
                        if(!this.validateName(this.state.userName)){
                            alert("Kindly Input Correct UserName")
                        }
                        if (!this.validateEmail(this.state.email)) {
                            // not a valid email
                            alert("Invalid Email Address")
                        }
                        if (!this.validatePassword(this.state.password)){
                            alert("Password Must be more than 6 Characters")
                        }
                        if (!this.validatePhone(this.state.phoneNumber)){
                            alert("Phone Number is not Valid")
                        }
                        else{
                            this.handleUpdateUser() 
                            this.goToAppMenu()                            
                        }                        
                        }}>
                        <Text style = {styles.submit}>
                            SAVE
                        </Text>
                    </TouchableHighlight>
                </KeyboardAvoidingView>




            </View>
        )


    }
};

const styles = StyleSheet.create({
    main:{
        // marginTop:22,
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#063e6a',



    },

    icon:{
        width: 30,
        height: 30,
        marginTop: 15,

    },

    username:{

        flexDirection: 'row',

    },



    image: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'white',
        paddingTop: '10%',
        paddingBottom:'10%'




    },

    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: '10%',
        flexDirection: 'row',
    },

    headtextview:{
        justifyContent: 'center',



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },

    backiconview:{

        justifyContent: 'center',


    },




    form: {
        justifyContent:'center',
        alignItems:'center',

    },
    Inputtext: {
        color:'white',
        fontSize: 15,
        margin: 3,
        height: 50,
        width: '80%',

        paddingRight: 10,
        paddingLeft: 10,

    },
    submitbutton: {
        justifyContent: 'center',
        //borderRadius:5,
        margin: 10,
        width: '70%',
        height: '15%',
        alignItems: 'center',
        borderWidth: 5,
        borderColor: '#ffa802',
        backgroundColor: '#003A67',
    },
    submit: {
        color:'white',
        fontSize:30,


    },
    bottom:{
        backgroundColor:'#003A67',
        alignItems:'center',

    },
    term:{
        color:'white',
        fontSize:15,
        paddingBottom:'20%',
    },
    userimageView:{
      //  borderWidth:2,
        borderColor:'#032f53',
        width:'50%',
        height:'27%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 75,
    },
    userTextView:{
        // borderWidth:3,
        // borderColor:"black",
        width:'50%',
        height:'5%',
        justifyContent:'center',
        alignItems:'center',

    },
    avatar: {
        borderRadius: 75,
        width: 155,
        height: 130
    }

});

function mapStateToProps(state) {
    return{
        userinfo:state.userinfo.userinfo
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({UpdateUserAction,ImageUploadAction},dispatch)
}

export default connect (mapStateToProps, mapDispatchToProps)(myAccount)