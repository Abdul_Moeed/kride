import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Landing from '../Landing';
import Login from '../Login'
import SignUp from '../SignUp';
import AppMenu from '../AppMenu';
import RideNow from '../RideNow';
import RideLater from '../RideLater';
import MyBooking from './mybooking';
import ContactUs from './contactus';
import Promotions from './Promotions';
import MYRIDE from './myRide';
import Wallet from './Wallet';
import ActivityIndicatorExample from '../ActivityIndicator'
import MYACCOUNT from './myAccount';
import addCreditCard from './addCreditCard';
import {Provider} from 'react-redux';
import {createStore,applyMiddleware} from 'redux';
import Reducers from '../redux/reducers/index'
import {logger} from 'redux-logger';
import thunk from 'redux-thunk';
import RideStart from '../RideStart';
import Billing from '../Billing';
import activityLogin from '../activityLogin'
import GooglePlacesAutoCompletePickup from '../GooglePlacesAutoCompletePickup'
import GooglePlacesAutoCompleteDropoff from '../GooglePlacesAutoCompleteDropoff'


const middleware = applyMiddleware(logger,thunk);
const store = createStore(Reducers, middleware);



const Routes = () => (
    <Provider store={store}>
    <Router>
        <Scene key="root">
            <Scene key = "activitylogin" component = {activityLogin} hideNavBar={true} initial={true}/>
            <Scene key = "landing" component = {Landing} hideNavBar={true} />
            <Scene key = "login" component = {Login} hideNavBar={true}  />
            <Scene key = "signup" component = {SignUp} hideNavBar={true}  />
            <Scene key = "appmenu" component = {AppMenu} hideNavBar={true}  />
            <Scene key = "ridelater" component = {RideLater} hideNavBar={true}  />
            <Scene key = "ridenow" component = {RideNow} hideNavBar={true}  />
            <Scene key = "booking" component = {MyBooking} hideNavBar={true}  />
            <Scene key ="Contactus" component = {ContactUs} hideNavBar = {true} />
            <Scene key ="promotions" component = {Promotions} hideNavBar = {true} />
            <Scene key ="myride" component = {MYRIDE} hideNavBar = {true} />
            <Scene key ="wallet" component = {Wallet} hideNavBar = {true} />
            <Scene key ="myaccount" component = {MYACCOUNT} hideNavBar = {true} />
            <Scene key ="card" component = {addCreditCard} hideNavBar = {true} />
            <Scene key ="ridestart" component = {RideStart} hideNavBar = {true} />
            <Scene key ="indicator" component = {ActivityIndicatorExample} hideNavBar = {true} />
            <Scene key ="billing" component = {Billing} hideNavBar = {true} />
            {/*<Scene key ="GooglePlacesPickup" component = {GooglePlacesAutoCompletePickup} hideNavBar = {true} />*/}
             <Scene key ="GooglePlacesDropoff" component = {GooglePlacesAutoCompleteDropoff} hideNavBar = {true} />

        </Scene>
    </Router>
    </Provider>
)
export default Routes
