import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    BackHandler,
    TextInput,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';
import DrawerLayout from 'react-native-drawer-layout'
import Menu from '../Menu';

import { Actions} from 'react-native-router-flux';



const image = require('../ImagesSources/menu.png');

const styles = StyleSheet.create({


    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,

        flexDirection: 'row',
    },



    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },


    button: {
        justifyContent: 'center',
        marginLeft:10,
        marginRight: 5,

    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        // marginTop:22,
    },
    main: {
        flex: 1,
        backgroundColor: '#073f6b',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection:'column'
    },
    options :{
        justifyContent:'center',
        height:70,
        width:'100%',
    },
    icon:{
        height:50,
        width:50,
        marginBottom:5,
    },
    buttonoption:{
      flexDirection:'row',
        borderBottomWidth:1,
        borderBottomColor:'black',
    },
    textview:{
        justifyContent:'center',
    },
    optiontext:{
      color: 'white',
      fontWeight: '800',
        fontSize:18,
      marginLeft:15,
        marginBottom:5,
    },

    imageview:{
        justifyContent:'center',
        marginLeft:20 ,

    },

    creditBalance:{
        height:'50%',
        width:'100%'
    },

    gridButtons:{
        flexDirection:'row',
        height:'50%',
        width:'100%',
        alignItems:'center',
    },
    bottomButton:{
        width:'40%',
        backgroundColor:'#032f53',
        height:'50%',
        margin:'5%',
        borderRadius:15,
        justifyContent:'center',
    },
    amountRemaining:{
        marginTop:'20%',
        textAlign:'center',
        color:'white',
        fontSize:30,
        fontWeight:'600',
        textDecorationLine:'underline',
    }

});


export default class Wallet extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){


        Actions.appmenu()
        return true;
    }


    render() {
        const goToCard = () => {
            Actions.card()

        }
        const menu = <Menu />;
        return (

            <DrawerLayout
                drawerWidth={220}
                ref={(drawer) => { return this.drawer = drawer }}
                keyboardDismissMode="on-drag"
                renderNavigationView={() => menu}>

                <View style={styles.container}>

                    <View style={styles.headbar}>

                        <View style={styles.button}>
                            <TouchableHighlight onPress={() => this.drawer.openDrawer()}>
                                <Image source={require('../ImagesSources/menu.png')} style={{ width: 32, height: 32}} />
                            </TouchableHighlight>
                        </View>

                        <View style={styles.headtextview} >
                            <Text style={styles.headtext}>Wallet</Text>
                        </View>


                    </View>

                    <View style={styles.main}>
                        <View style = {styles.creditBalance}>

                            <Text style = {{marginTop:'5%',color:'white', textAlign:'center', fontWeight:'500',fontSize:15}}> You Have this much Balance </Text>
                                <Text style = {styles.amountRemaining}>Not Available</Text>
                        </View>
                        <View style = {styles.gridButtons}>

                            <TouchableHighlight style = {styles.bottomButton} onPress = {alert('Service is not available Now')} >
                                <Text style = {{fontSize:20, fontWeight:'900', color:'white', textAlign:'center'}}> CREDIT BALANCE </Text>
                            </TouchableHighlight>

                            <TouchableHighlight style = {styles.bottomButton} onPress = {alert('Wallet Is coming Soon')}>
                                <Text style = {{fontSize:20,fontWeight:'900', color:'white',textAlign:'center'}}> ADD {'\n'} CREDIT CARD </Text>
                            </TouchableHighlight>



                        </View>

                    </View>

                </View>


            </DrawerLayout>

        );


    }

}

