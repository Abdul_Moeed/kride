import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    BackHandler,
    Image,
    ImageBackground,
    AsyncStorage,
    ScrollView,
    TextInput,
    TouchableHighlight,
} from 'react-native';
import DrawerLayout from 'react-native-drawer-layout'
import Menu from '../Menu';

import { Actions} from 'react-native-router-flux';



const image = require('../ImagesSources/menu.png');

const styles = StyleSheet.create({


    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,

        flexDirection: 'row',
    },



    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,
    },
    headtext:{
        color:'white',
        fontSize: 20,
    },
    button: {
        justifyContent: 'center',
        marginLeft:10,
        marginRight: 5,
    },

    container: {
        // marginTop:22,
        flex: 1,
    },
    main: {
        flex: 1,
        backgroundColor: 'black',
      },
      backgroundImage:{
        flex: 1,
        justifyContent:'center',
              resizeMode: 'cover',
              width: '100%',
              height: '100%',


      },

});


export default class Promotions extends Component {

    constructor(props) {
        super(props);

    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){


        Actions.appmenu()
        return true;
    }



    render() {

        const menu = <Menu />;
        return (


            <DrawerLayout
                drawerWidth={220}
                ref={(drawer) => { return this.drawer = drawer }}
                keyboardDismissMode="on-drag"
                renderNavigationView={() => menu}>

            <View style={styles.container}>

                    <View style={styles.headbar}>

                        <View style={styles.button}>
                            <TouchableHighlight onPress={() => this.drawer.openDrawer()}>
                                <Image source={require('../ImagesSources/menu.png')} style={{ width: 32, height: 32}} />
                            </TouchableHighlight>
                        </View>

                        <View style={styles.headtextview} >
                            <Text style={styles.headtext}>Promotions</Text>
                        </View>
                    </View>
                    <View style={styles.main}>

                    <Image style={styles.backgroundImage} source = {require('../ImagesSources/background.png')}>
                    <View style={{alignItems:'center'}}>
                    <Text style={{color:'white', fontSize:18,backgroundColor:'transparent'}}> There is no any Promotion Right Now</Text>
                    </View>
                    </Image>
                    </View>



                    </View>





            </DrawerLayout>

        );


    }

}
