import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ImageBackground,
    ScrollView,
    ListView,
    AsyncStorage,
    Alert,
    TextInput,
    BackHandler,
    ToastAndroid,
    TouchableHighlight,
} from 'react-native';
import DrawerLayout from 'react-native-drawer-layout'
import Menu from '../Menu';
import { Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {myBookingAction} from '../redux/actions/myBookingAction'

const image = require('../ImagesSources/menu.png');

const styles = StyleSheet.create({


    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: 50,
        flexDirection: 'row',
    },



    headtextview:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:10,



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },


    button: {
        justifyContent: 'center',
        marginLeft:10,
        marginRight: 5,

    },
    caption: {
        fontSize: 20,
        fontWeight: 'bold',
        alignItems: 'center',
    },
    container: {
        // marginTop:22,
        flex: 1,
    },
    main: {

        flex: 1,
        backgroundColor: 'black',
      },
backgroundImage:{
  flex: 1,
        resizeMode: 'cover',
        width: '100%',
        height: '100%',


},
listbox:{
  marginBottom: 7,
//   flexDirection:'row',
  backgroundColor: 'white',
  width: View.width,
  height: 200,
  justifyContent:'center'
},
iconV:{
  alignItems:'flex-start',
  justifyContent:'center',
  width: '55%',
},
icon: {
  borderRadius:25,
  width:40,
  height:40,
},
infoV:{
  width: '45%',
  justifyContent:'center',
  borderLeftWidth:1,
  borderLeftColor:'grey',
},
info:{
  margin: 5,
}


});

class mybooking extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        AsyncStorage.getItem('_id').then((userid) => {
            const myBooking = {
                userid:userid
            }
            this.props.myBookingAction(myBooking)
        })
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

    }

    handleBackButton(){


        Actions.appmenu()
        return true;
    }

    render() {

        const menu = <Menu />;

        return (


            <DrawerLayout
                drawerWidth={220}
                ref={(drawer) => { return this.drawer = drawer }}
                keyboardDismissMode="on-drag"
                renderNavigationView={() => menu}>
                <View style={styles.container}>

                    <View style={styles.headbar}>

                        <View style={styles.button}>
                            <TouchableHighlight onPress={() => this.drawer.openDrawer()}>
                                <Image source={require('../ImagesSources/menu.png')} style={{ width: 32, height: 32}} />
                            </TouchableHighlight>
                        </View>
                        <View style={styles.headtextview} >
                            <Text style={styles.headtext}>My BOOKING</Text>
                        </View>
                    </View>
                    <View style={styles.main}>

                    <Image style={styles.backgroundImage} source = {require('../ImagesSources/background.png')}>
                        <ScrollView>
                        {
                            this.props.myBooking.map((x) => {
                            return(

                                <View key={x._id} style={styles.listbox}>
                                        <View style={{flexDirection:'row'}}>
                                        <Image style={{width:25, height:25}} source={require('../ImagesSources/upArrow.png')}/>
                                        <Text>{x.pickuplocation}</Text>
                                        </View>
                                        <View style={{flexDirection:'row'}}>
                                        <Image style={{width:25, height:25}} source={require('../ImagesSources/downArrow.png')}/>
                                        <Text>{x.dropofflocation}</Text>
                                        </View>                                 
                                        <Text style={{marginLeft:25, marginTop:3}}>Book Time: {x.bookTime}</Text>
                                        <Text style={{marginLeft:25, marginTop:3}}>Pickup Time: {x.pickupTime}</Text>
                                </View>

                            )
                        })
                        }

                        </ScrollView>

                    </Image>
                    </View>


                </View>


            </DrawerLayout>

        );


    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({myBookingAction},dispatch)
}

function mapStateToProps(state) {
    return{
        myBooking:state.myBooking.myBooking
    }
}
export default connect (mapStateToProps, mapDispatchToProps)(mybooking)