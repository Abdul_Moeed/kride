import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    BackHandler,
    TextInput,
    Image,
    View,
    ScrollView,
    AsyncStorage,
    KeyboardAvoidingView,
    TouchableHighlight
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {addCreditCardAction} from '../redux/actions/addCreditCardAction'



class addCreditCard extends Component {
    constructor(){
        super()
        this.state = {
            userid:'',
            cardNumber:'',
            expiresDate:'',
            cardType:"",
            cardcvv:'',
            cardholderName:''
        }
    }

    handleCreditCard(){
        const addCreditCard = {
            userId:this.state.userid,
            cardNumber:this.state.cardNumber,
            cardType:this.state.cardType,
            expirationDate:this.state.expiresDate,
            cardcvv:this.state.cardcvv,
            username:this.state.cardholderName
        }
        this.props.addCreditCardAction(addCreditCard)
        // console.log(addCreditCard)
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        AsyncStorage.getItem("_id").then((x) => {
            this.setState({userid:x})
            // console.log(x)
        })
    }

    componentWillMount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton(){


        Actions.wallet()
        return true;
    }
    render() {

        const goToBack = () => {
            Actions.wallet()
        }

        return (
            <View style={styles.main}>

                <View style={styles.headbar}>
                    <View style={styles.backiconview}>
                        <TouchableHighlight onPress = {goToBack}>
                            <Image style={styles.backicon} source = {require('../ImagesSources/back.png')}/>
                        </TouchableHighlight>
                    </View>

                    <View style={styles.headtextview} >
                        <Text style={styles.headtext}>ADD CREDIT CARD</Text>
                    </View>
                </View>
                <ScrollView>






                    <KeyboardAvoidingView style={styles.form}>


                        <View style={styles.username}>
                            <TextInput style={styles.Inputtext}
                                       placeholder="CARD NUMBER"
                                       placeholderTextColor="silver"
                                       autoCapitalize="none"
                                       onChangeText={(text) => this.setState({cardNumber:text})}
                            />

                        </View>

                        <View style={styles.username}>
                            <TextInput style={styles.Inputtext}
                                       placeholder="DD/MM/YYYY"
                                       placeholderTextColor="silver"
                                       autoCapitalize="none"
                                       onChangeText={(text) => this.setState({expiresDate:text})}
                            />
                        </View>

                        <View style={styles.username}>
                            <TextInput style={styles.Inputtext}
                                       placeholder="Card Type"
                                       placeholderTextColor="silver"
                                       autoCapitalize="none"
                                       onChangeText={(text) => this.setState({cardType:text})}
                            />
                        </View>

                        <View style={styles.username}>

                            <TextInput style={styles.Inputtext}
                                       placeholder="CVV CODE"
                                       placeholderTextColor="silver"
                                       autoCapitalize="none"
                                       onChangeText={(text) => this.setState({cardcvv:text})}
                            />
                        </View>

                        <View style={styles.username}>

                            <TextInput style={styles.Inputtext}
                                       placeholder="CARD HOLDER NAME"
                                       placeholderTextColor="silver"
                                       autoCapitalize="none"
                                       onChangeText={(text) => this.setState({cardholderName:text})}
                            />
                        </View>


                        <TouchableHighlight onPress={() => this.handleCreditCard()} style = {styles.submitbutton} >
                            <Text style = {styles.submit}>
                                DONE
                            </Text>
                        </TouchableHighlight>
                    </KeyboardAvoidingView>



                </ScrollView>
            </View>
        )


    }
};

const styles = StyleSheet.create({
    main:{
        // marginTop:22,
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#063e6a',



    },



    username:{

        flexDirection: 'row',

    },



    image: {
        justifyContent:'center',
        alignItems:'center',
        backgroundColor: 'white',
        paddingTop: '10%',
        paddingBottom:'10%'




    },

    headbar:{
        backgroundColor:'#032f53',
        width: View.width,
        height: '10%',
        flexDirection: 'row',
    },

    headtextview:{
        justifyContent: 'center',



    },
    headtext:{
        color:'white',
        fontSize: 20,


    },

    backiconview:{

        justifyContent: 'center',


    },




    form: {
        justifyContent:'center',
        alignItems:'center',

    },
    Inputtext: {
        color:'white',
        fontSize: 15,
        margin: 3,
        height: 50,
        width: '80%',

        paddingRight: 10,
        paddingLeft: 10,

    },
    submitbutton: {
        justifyContent: 'center',
        //borderRadius:5,
        margin: 10,
        width: '70%',
        height: '15%',
        alignItems: 'center',
        borderWidth: 5,
        borderColor: '#ffa802',
        backgroundColor: '#003A67',
    },
    submit: {
        color:'white',
        fontSize:30,


    },
    bottom:{
        backgroundColor:'#003A67',
        alignItems:'center',

    },
    term:{
        color:'white',
        fontSize:15,
        paddingBottom:'20%',
    }
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addCreditCardAction},dispatch)
}


export default connect (null, mapDispatchToProps) (addCreditCard)